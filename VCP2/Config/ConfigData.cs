﻿using SSLib.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Config
{
    public class ConfigData
    {
        private static string ConfigPath = @"Config/VCP.option";

        #region Singletone
        private static ConfigData instance;
        public static ConfigData Instance
        {
            get
            {
                if (instance == null)
                    instance = XmlSerializerHelper.Load(ConfigPath, new ConfigData());
                return instance;
            }
        }
        #endregion

        #region Default Function
        public static void Save() => XmlSerializerHelper.Save(Instance, ConfigPath);

        public static void Refresh() => instance = default(ConfigData);
        #endregion

        /// <summary>
        /// Simulation Mode에서 PLC값 임의로 생성해서 Return
        /// </summary>
        public bool IsSimulationMode { get; set; }

        /// <summary>
        /// Vehicle Number 
        /// </summary>
        public short VehicleNumber { get; set; }
        public string ServerIP { get; set; }
        public int ServerPort { get; set; }
        public int MaxNodeCount { get; set; }
        public int LastStartPoint { get; set; }
        public int LastEndPoint { get; set; }

    }
}
