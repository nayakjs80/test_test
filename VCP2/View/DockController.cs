﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.View
{
    public sealed class DockController
    {
        #region Singletone
        private static readonly DockController instance = new DockController();

        public static DockController Instance { get { return instance; } }
        #endregion

        public StatusForm Status { get; private set; } = new StatusForm();

        public PLCForm PLC { get; private set; } = new PLCForm();

        public IOForm IO { get; private set; } = new IOForm();

        public ErrorForm Error { get; private set; } = new ErrorForm();

        private DockController()
        {
            Status.CloseButton = false;
            PLC.CloseButton = false;
            IO.CloseButton = false;
            Error.CloseButton = false;
            Status.CloseButtonVisible = false;
            PLC.CloseButtonVisible = false;
            IO.CloseButtonVisible = false;
            Error.CloseButtonVisible = false;
        }

        public void ShowAll()
        {
            Status.Show();
            PLC.Show();
            IO.Show();
            Error.Show();
        }

        public void HideAll()
        {
            Status.Hide();
            PLC.Hide();
            IO.Hide();
            Error.Hide();
        }
    }
}
