﻿using OCS.VCP.Controller;
using OCS.VCP.Model.TagSet;
using OCS.VCP.Model.ViewModel;
using SSLib.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OCS.VCP.View.WPF
{
    /// <summary>
    /// StatusView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class StatusView : UserControl
    {
        public StatusView()
        {
            InitializeComponent();

            left1.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_1_LEFT);
            left2.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_2_LEFT);
            left3.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_3_LEFT);
            left4.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_4_LEFT);

            right1.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_1_RIGHT);
            right2.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_2_RIGHT);
            right3.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_3_RIGHT);
            right4.DataContext = IOViewModel.Instance.IOCollection.Single(io => io.Index == (int)VehicleIO.DIRECTION_4_RIGHT);

            this.DataContext = StatusViewModel.Instance;
        }
    }
}
