﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace OCS.VCP.View
{
    public partial class IOForm : DockContent
    {
        public IOForm()
        {
            InitializeComponent();
            
        }

        internal void SetVisible(bool visible)
        {
            if (visible)
                Show();
            else
                Hide();
        }
    }
}
