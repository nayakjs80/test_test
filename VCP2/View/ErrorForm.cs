﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace OCS.VCP.View
{
    public partial class ErrorForm : DockContent
    {
        public ErrorForm()
        {
            InitializeComponent();
        }

        public void SetVisible(bool visible)
        {
            if (visible)
                Show();
            else
                Hide();
        }

        private void ErrorForm_Load(object sender, EventArgs e)
        {

        }
        
    }
}
