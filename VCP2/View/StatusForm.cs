﻿using OCS.VCP.Controller;
using OCSProtocol.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace OCS.VCP.View
{
    public partial class StatusForm : DockContent
    {
        public StatusForm()
        {
            InitializeComponent();
        }

        public void Initializes()
        {
            StatusTimer.Enabled = true;
        }

        public void SetVisible(bool visible)
        {
            if (visible)
                Show();
            else
                Hide();
        }

        private void StatusTimer_Tick(object sender, EventArgs e)
        {
        }
        
    }
}
