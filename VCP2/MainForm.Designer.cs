﻿using OCS.VCP.View;

namespace OCS.VCP
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pLCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.VCPStatusTimer = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.spPlcActive = new SSLib.Controls.StatusPanel();
            this.spCIMConnect = new SSLib.Controls.StatusPanel();
            this.spMode = new SSLib.Controls.StatusPanel();
            this.spState = new SSLib.Controls.StatusPanel();
            this.spCSTContain = new SSLib.Controls.StatusPanel();
            this.spSoonArrive = new SSLib.Controls.StatusPanel();
            this.spCmdEnable = new SSLib.Controls.StatusPanel();
            this.estopButton = new System.Windows.Forms.Button();
            this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1227, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(108, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pLCToolStripMenuItem,
            this.statusToolStripMenuItem,
            this.iOToolStripMenuItem,
            this.errorListToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(54, 24);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // pLCToolStripMenuItem
            // 
            this.pLCToolStripMenuItem.Name = "pLCToolStripMenuItem";
            this.pLCToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.pLCToolStripMenuItem.Text = "PLC";
            this.pLCToolStripMenuItem.Click += new System.EventHandler(this.PLCToolStripMenuItem_Click);
            // 
            // statusToolStripMenuItem
            // 
            this.statusToolStripMenuItem.Name = "statusToolStripMenuItem";
            this.statusToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.statusToolStripMenuItem.Text = "Status";
            this.statusToolStripMenuItem.Click += new System.EventHandler(this.StatusToolStripMenuItem_Click);
            // 
            // iOToolStripMenuItem
            // 
            this.iOToolStripMenuItem.Name = "iOToolStripMenuItem";
            this.iOToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.iOToolStripMenuItem.Text = "IO";
            this.iOToolStripMenuItem.Click += new System.EventHandler(this.iOToolStripMenuItem_Click);
            // 
            // errorListToolStripMenuItem
            // 
            this.errorListToolStripMenuItem.Name = "errorListToolStripMenuItem";
            this.errorListToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.errorListToolStripMenuItem.Text = "Error List";
            this.errorListToolStripMenuItem.Click += new System.EventHandler(this.errorListToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Location = new System.Drawing.Point(0, 729);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 13, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1227, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // VCPStatusTimer
            // 
            this.VCPStatusTimer.Enabled = true;
            this.VCPStatusTimer.Tick += new System.EventHandler(this.VCPStatusTimer_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1MinSize = 50;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dockPanel);
            this.splitContainer1.Size = new System.Drawing.Size(1227, 701);
            this.splitContainer1.SplitterDistance = 69;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Controls.Add(this.spPlcActive, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.spCIMConnect, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.spMode, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.spState, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.spCSTContain, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.spSoonArrive, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.spCmdEnable, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.estopButton, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.MaximumSize = new System.Drawing.Size(0, 50);
            this.tableLayoutPanel1.MinimumSize = new System.Drawing.Size(0, 50);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1227, 50);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // spPlcActive
            // 
            this.spPlcActive.Active = true;
            this.spPlcActive.BackColor = System.Drawing.Color.GreenYellow;
            this.spPlcActive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spPlcActive.Location = new System.Drawing.Point(156, 3);
            this.spPlcActive.Name = "spPlcActive";
            this.spPlcActive.Size = new System.Drawing.Size(147, 44);
            this.spPlcActive.TabIndex = 0;
            this.spPlcActive.Title = "";
            this.spPlcActive.ToolTipText = null;
            // 
            // spCIMConnect
            // 
            this.spCIMConnect.Active = true;
            this.spCIMConnect.BackColor = System.Drawing.Color.GreenYellow;
            this.spCIMConnect.Cursor = System.Windows.Forms.Cursors.Default;
            this.spCIMConnect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spCIMConnect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.spCIMConnect.Location = new System.Drawing.Point(309, 3);
            this.spCIMConnect.Name = "spCIMConnect";
            this.spCIMConnect.Size = new System.Drawing.Size(147, 44);
            this.spCIMConnect.TabIndex = 1;
            this.spCIMConnect.Title = "";
            this.spCIMConnect.ToolTipText = null;
            // 
            // spMode
            // 
            this.spMode.Active = true;
            this.spMode.BackColor = System.Drawing.Color.GreenYellow;
            this.spMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spMode.Location = new System.Drawing.Point(462, 3);
            this.spMode.Name = "spMode";
            this.spMode.Size = new System.Drawing.Size(147, 44);
            this.spMode.TabIndex = 3;
            this.spMode.Title = "";
            this.spMode.ToolTipText = null;
            // 
            // spState
            // 
            this.spState.Active = true;
            this.spState.BackColor = System.Drawing.Color.GreenYellow;
            this.spState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spState.Location = new System.Drawing.Point(615, 3);
            this.spState.Name = "spState";
            this.spState.Size = new System.Drawing.Size(147, 44);
            this.spState.TabIndex = 4;
            this.spState.Title = "";
            this.spState.ToolTipText = null;
            // 
            // spCSTContain
            // 
            this.spCSTContain.Active = true;
            this.spCSTContain.BackColor = System.Drawing.Color.GreenYellow;
            this.spCSTContain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spCSTContain.Location = new System.Drawing.Point(768, 3);
            this.spCSTContain.Name = "spCSTContain";
            this.spCSTContain.Size = new System.Drawing.Size(147, 44);
            this.spCSTContain.TabIndex = 5;
            this.spCSTContain.Title = null;
            this.spCSTContain.ToolTipText = null;
            // 
            // spSoonArrive
            // 
            this.spSoonArrive.Active = true;
            this.spSoonArrive.BackColor = System.Drawing.Color.GreenYellow;
            this.spSoonArrive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spSoonArrive.Location = new System.Drawing.Point(921, 3);
            this.spSoonArrive.Name = "spSoonArrive";
            this.spSoonArrive.Size = new System.Drawing.Size(147, 44);
            this.spSoonArrive.TabIndex = 6;
            this.spSoonArrive.Title = null;
            this.spSoonArrive.ToolTipText = null;
            // 
            // spCmdEnable
            // 
            this.spCmdEnable.Active = true;
            this.spCmdEnable.BackColor = System.Drawing.Color.GreenYellow;
            this.spCmdEnable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spCmdEnable.Location = new System.Drawing.Point(1074, 3);
            this.spCmdEnable.Name = "spCmdEnable";
            this.spCmdEnable.Size = new System.Drawing.Size(150, 44);
            this.spCmdEnable.TabIndex = 7;
            this.spCmdEnable.Title = null;
            this.spCmdEnable.ToolTipText = null;
            // 
            // estopButton
            // 
            this.estopButton.BackColor = System.Drawing.Color.Red;
            this.estopButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.estopButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.estopButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.estopButton.Font = new System.Drawing.Font("Consolas", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estopButton.ForeColor = System.Drawing.Color.White;
            this.estopButton.Location = new System.Drawing.Point(3, 3);
            this.estopButton.Name = "estopButton";
            this.estopButton.Size = new System.Drawing.Size(147, 44);
            this.estopButton.TabIndex = 8;
            this.estopButton.Text = "E-STOP";
            this.estopButton.UseVisualStyleBackColor = false;
            this.estopButton.Click += new System.EventHandler(this.estopButton_Click);
            // 
            // dockPanel
            // 
            this.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel.DockBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(242)))));
            this.dockPanel.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPanel.Location = new System.Drawing.Point(0, 0);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.ShowAutoHideContentOnHover = false;
            this.dockPanel.Size = new System.Drawing.Size(1227, 631);
            this.dockPanel.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 751);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainForm";
            this.Text = "VCP";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pLCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Timer VCPStatusTimer;
        private System.Windows.Forms.ToolStripMenuItem iOToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private SSLib.Controls.StatusPanel spPlcActive;
        private SSLib.Controls.StatusPanel spCIMConnect;
        private SSLib.Controls.StatusPanel spMode;
        private SSLib.Controls.StatusPanel spState;
        private SSLib.Controls.StatusPanel spCSTContain;
        private SSLib.Controls.StatusPanel spSoonArrive;
        private SSLib.Controls.StatusPanel spCmdEnable;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        private System.Windows.Forms.Button estopButton;
        private System.Windows.Forms.ToolStripMenuItem errorListToolStripMenuItem;
    }
}

