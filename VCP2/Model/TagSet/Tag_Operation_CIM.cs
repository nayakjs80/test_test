﻿using SSLib.Extentions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.TagSet
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct Tag_Operation_CIM : IOmronTag
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public byte[] Operation;
        //type : BOOL
        //Operation[0] : IPC_Power_On_Req 
        //Operation[1] : IPC_Power_Off_Req 
        //Operation[2] : IPC_Auto_Req 
        //Operation[3] : IPC_Stop_Req  (IPC > PLC 비상정지 신호 해당 신호 On 시 X-Axis 기동정지)
        //Operation[4] : IPC_Initial_Req 
        //Operation[5] : IPC_Estop_Req 
        //Operation[6] : IPC_Err_Reset_Req 
        //Operation[7] : IPC_Pause_Req  


        public byte[] Serialize()
        {
            var buffer = new byte[Marshal.SizeOf(typeof(Tag_Command))];
            var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            var pBuffer = gch.AddrOfPinnedObject();

            Marshal.StructureToPtr(this, pBuffer, false);
            gch.Free();
            buffer.Swap();

            return buffer;
        }

        public void Deserialize(byte[] data)
        {
            data.Swap();
            var gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            this = (Tag_Operation_CIM)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Tag_Operation_CIM));
            gch.Free();
        }
    }
}
