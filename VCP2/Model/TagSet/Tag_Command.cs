﻿using SSLib.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.TagSet
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct Tag_Command : IOmronTag
    {


        [MarshalAs(UnmanagedType.I2)]
        public short Cmd_Code; //Go:300, GoMore:350, Load:100, Unload:200, 이니셜:400(X-Axis Barcode Search)
        [MarshalAs(UnmanagedType.I4)]
        public int Order_Number;//MCS Order 관련 ID (사이즈 협의) CST ID
        [MarshalAs(UnmanagedType.I2)]
        public short Cmd_Number;//0~9 순환, Cmd 구분용
        [MarshalAs(UnmanagedType.I2)]
        public short Start_Node;//작업 시작 위치
        [MarshalAs(UnmanagedType.I2)]
        public short Target_Node; //Go, GoMore일 경우 최종목표 위치
        [MarshalAs(UnmanagedType.I2)]
        public short Total_Node; //Go, GoMore일 경우 총 이동 노드(존)
        [MarshalAs(UnmanagedType.I4)]
        public int Total_Distance; //Go, GoMore일 경우 총 이동 거리
        [MarshalAs(UnmanagedType.I2)]
        public short Section_Count; //"Go, GoMore일 경우 나갈 총 섹션 개수 (5개 이상의 섹션일 경우, 대기하고있다가 버퍼가 비면 채워줄 예정입니다)"
        [MarshalAs(UnmanagedType.I2)]
        public short Direction_Info; //1:왼쪽 2:오른쪽
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
        public byte[] From_Port; //From Node 이름
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
        public byte[] To_Port; //To Node 이름
        [MarshalAs(UnmanagedType.I2)]
        public short Moving_Type; //bool이지만 구조체 안의 연속되지않은 bool은 2byte로 넘어옴 (0 or 1) //Command 생성 후 이동 할 경우 On, 밀어내기를 당하는 경우 Off
        [MarshalAs(UnmanagedType.I2)]
        public short Active_Pio_NodeNumber;

        public byte[] Serialize()
        {
            To_Port.Swap();
            From_Port.Swap();

            var buffer = new byte[Marshal.SizeOf(typeof(Tag_Command))];
            var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            var pBuffer = gch.AddrOfPinnedObject();

            Marshal.StructureToPtr(this, pBuffer, false);
            gch.Free();
            buffer.Swap();

            return buffer;
        }

        public void Deserialize(byte[] data)
        {
            data.Swap();

            var gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            this = (Tag_Command)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Tag_Command));
            gch.Free();

            To_Port.Swap();
            From_Port.Swap();
        }

        public void Initialize()
        {
            this.Active_Pio_NodeNumber = 0;
            this.Cmd_Code = 0;
            this.Cmd_Number = 0;
            this.Direction_Info = 0;
            this.Moving_Type = 0;
            this.Order_Number = 0;
            this.Section_Count = 0;
            this.Start_Node = 0;
            this.Target_Node = 0;
            this.Total_Distance = 0;
            this.Total_Node = 0;
            this.To_Port = new byte[18];
            this.From_Port = new byte[18];
        }
    }
}
