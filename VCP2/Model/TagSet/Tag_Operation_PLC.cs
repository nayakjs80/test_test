﻿using SSLib.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.TagSet
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct Tag_Operation_PLC : IOmronTag
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public byte[] Operation;

        //Operation[0] : VC_Power_On_Enable
        //Operation[1] : VC_Power_On_Status
        //Operation[2] : VC_Power_Off_Enable
        //Operation[3] : VC_Power_Off_Status
        //Operation[4] : VC_Auto_Enable
        //Operation[5] : VC_Auto_Status
        //Operation[6] : VC_Stop_Enable
        //Operation[7] : VC_Stop_Status
        //Operation[8] : VC_Initial_Enable
        //Operation[9] : VC_Initial_Status
        //Operation[10] : VC_From_Transfer_Enable
        //Operation[11] : VC_To_Transfer_Enable



        public byte[] Serialize()
        {
            var buffer = new byte[Marshal.SizeOf(typeof(Tag_Command))];
            var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            var pBuffer = gch.AddrOfPinnedObject();

            Marshal.StructureToPtr(this, pBuffer, false);
            gch.Free();
            buffer.Swap();

            return buffer;
        }

        public void Deserialize(byte[] data)
        {
            data.Swap();
            var gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            this = (Tag_Operation_PLC)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Tag_Operation_PLC));
            gch.Free();
        }
    }
}
