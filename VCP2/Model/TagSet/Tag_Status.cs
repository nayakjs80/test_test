﻿using SSLib.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.TagSet
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct Tag_Status : IOmronTag, ICloneable
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
        public byte[] IO;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Status;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Regulator_Input_Voltage1;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Regulator_Input_Voltage2;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Regulator_Output_Voltage;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Regulator_Output_Ampare_1;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Regulator_Output_Ampare_2;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Regulator_Inside_Temp;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_ECO_CAP_Voltage;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_ECO_Ampare;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_REG_ECO_Total_Ampare;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_ECO_Inside_Temp;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_PickUp_1_Temp;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_PickUp_2_Temp;
        [MarshalAs(UnmanagedType.R4)]
        public float CPS_Error_Code;

        [MarshalAs(UnmanagedType.I2)]//Core전달시 StartPoint
        public short Before_Node;
        [MarshalAs(UnmanagedType.I2)]//Core전달시 EndPoint //정지시:현재 위치, 이동중:다음 위치
        public short Current_Node;
        [MarshalAs(UnmanagedType.I2)] // bool이지만 구조체 안의 연속되지않은 bool은 2byte로 넘어옴 (0 or 1)
        public short Vehicle_Position;
        [MarshalAs(UnmanagedType.I2)]//주행중 지나온 노드(존) 개수
        public short Past_Node_Count;
        [MarshalAs(UnmanagedType.I2)] //bool이지만 구조체 안의 연속되지않은 bool은 2byte로 넘어옴 (0 or 1) 
        public short CMD_Enable;      //이니셜 완료 후 반송가능 - IDLE 상태확인, Error 없음확인, Vehicle 정위치 확인
        [MarshalAs(UnmanagedType.I4)]//현재 주행 거리 - 실시간
        public int Drive_Distance;
        [MarshalAs(UnmanagedType.I4)]//이전 주행 거리
        public int Previous_Distance;
        [MarshalAs(UnmanagedType.I2)]
        public short Drive_Speed;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]  // 구조체 안의 연속된 bool은 2byte 안에 포함 // bool CST_Contain, bool SoonArrive
        public byte[] ExtraStatus;
        
        [MarshalAs(UnmanagedType.I2)]//Manual=1 Auto=2
        public short Vcle_Mode;
        [MarshalAs(UnmanagedType.I2)]//주행중 100, 주행완료 101 로딩중 300 로딩완료 301 언로딩중 400 언로딩완료 401 Init 중  500 Init 완료 501 에러 999
        public short Vcle_Status;  
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 75)]//0~599 bool
        public byte[] ErrorCode;
        [MarshalAs(UnmanagedType.I2)]//MCUL 관련 항목 추가 필요(MCUL항목 파악 필요)
        public short MCUL_Status;
        [MarshalAs(UnmanagedType.I2)]//MCUL 관련 항목 추가 필요(MCUL항목 파악 필요)
        public short MCUL_SetSpeed;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
        public byte[] CstId;

        public byte[] Serialize()
        {
            CstId.Swap();

            var buffer = new byte[Marshal.SizeOf(typeof(Tag_Status))];
            var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            var pBuffer = gch.AddrOfPinnedObject();

            Marshal.StructureToPtr(this, pBuffer, false);
            gch.Free();
            buffer.Swap();

            return buffer;
        }

        public void Deserialize(byte[] data)
        {
            data.Swap();
            var gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            this = (Tag_Status)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Tag_Status));
            gch.Free();

            CstId.Swap();
        }

        public object Clone()
        {
            Tag_Status clone = new Tag_Status
            {
                Before_Node = this.Before_Node,
                CMD_Enable = this.CMD_Enable,
                CPS_ECO_Ampare = this.CPS_ECO_Ampare,
                CPS_ECO_CAP_Voltage = this.CPS_ECO_CAP_Voltage,
                CPS_ECO_Inside_Temp = this.CPS_ECO_Inside_Temp,
                CPS_Error_Code = this.CPS_Error_Code,
                CPS_PickUp_1_Temp = this.CPS_PickUp_1_Temp,
                CPS_PickUp_2_Temp = this.CPS_PickUp_2_Temp,
                CPS_Regulator_Input_Voltage1 = this.CPS_Regulator_Input_Voltage1,
                CPS_Regulator_Input_Voltage2 = this.CPS_Regulator_Input_Voltage2,
                CPS_Regulator_Inside_Temp = this.CPS_Regulator_Inside_Temp,
                CPS_Regulator_Output_Ampare_1 = this.CPS_Regulator_Output_Ampare_1,
                CPS_Regulator_Output_Ampare_2 = this.CPS_Regulator_Output_Ampare_2,
                CPS_Regulator_Output_Voltage = this.CPS_Regulator_Output_Voltage,
                CPS_REG_ECO_Total_Ampare = this.CPS_REG_ECO_Total_Ampare,
                CPS_Status = this.CPS_Status,
                CstId = this.CstId,
                Current_Node = this.Current_Node,
                Drive_Distance = this.Drive_Distance,
                Drive_Speed = this.Drive_Speed,
                ErrorCode = this.ErrorCode,
                ExtraStatus = this.ExtraStatus,
                IO = this.IO,
                MCUL_SetSpeed = this.MCUL_SetSpeed,
                MCUL_Status = this.MCUL_Status,
                Past_Node_Count = this.Past_Node_Count,
                Previous_Distance = this.Previous_Distance,
                Vcle_Mode = this.Vcle_Mode,
                Vcle_Status = this.Vcle_Status,
                Vehicle_Position = this.Vehicle_Position
            };
            return clone;
        }

    }
}
