﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.TagSet
{
    public interface IOmronTag
    {
        byte[] Serialize();
        void Deserialize(byte[] data);
    }
}
