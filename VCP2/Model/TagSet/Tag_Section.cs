﻿using SSLib.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.TagSet
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct Tag_Section : IOmronTag
    {
        [MarshalAs(UnmanagedType.I2)]
        public short Speed;
        [MarshalAs(UnmanagedType.I4)]
        public int Distance;
        [MarshalAs(UnmanagedType.I2)]
        public short Drive_Type;
        [MarshalAs(UnmanagedType.I2)]
        public short Steer_Direction;
        [MarshalAs(UnmanagedType.I2)]
        public short NextSteer_Direction;
        [MarshalAs(UnmanagedType.I2)]
        public short Node_Count;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public short[] Node_Number;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public ushort[] Node_Distance;
        [MarshalAs(UnmanagedType.I2)]
        public short Section_Idx;

        public byte[] Serialize()
        {
            var buffer = new byte[Marshal.SizeOf(typeof(Tag_Section))];
            var gch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            var pBuffer = gch.AddrOfPinnedObject();

            Marshal.StructureToPtr(this, pBuffer, false);
            gch.Free();
            buffer.Swap();

            return buffer;
        }

        public void Deserialize(byte[] data)
        {
            data.Swap();
            var gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            this = (Tag_Section)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(Tag_Section));
            gch.Free();
        }

        public void Initialize()
        {
            this.Distance = 0;
            this.Drive_Type = 0;
            this.NextSteer_Direction = 0;
            this.Node_Count = 0;
            this.Section_Idx = 0;
            this.Speed = 0; 
            this.Steer_Direction = 0;

            this.Node_Distance = new ushort[20];
            this.Node_Number = new short[20];
        }
    }

}
