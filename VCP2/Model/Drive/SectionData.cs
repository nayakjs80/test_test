﻿using SSDatabase.Models.RunData;
using SSLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.Drive
{
    public class SectionData : ICloneable
    {
        // Variable -----
        public int StartNodeNumber;
        public int EndNodeNumber;
        public LineArc LinkType;
        public SteerDir SteerDirection;
        public SteerDir NextSteerDirection;
        public int SpeedOrg;
        public int Speed;
        public int Distance;
        public int tagStartIndex;
        public int tagDecelIndex;
        public int tagEndIndex;

        public short Section_Idx;
        public bool Alloc;
        public int AllocIdx;

        ///  주행할 포인트 시작~목표
        public List<Point> DrivePointList = new List<Point>();
        /// 주행할 세그먼트        
        public List<Segment> DriveSegmentList = new List<Segment>();

        // Function
        public void Reset()
        {
            LinkType = LineArc.Line;
            SteerDirection = SteerDir.DontCare;
            NextSteerDirection = SteerDir.DontCare;
            StartNodeNumber = 0;
            EndNodeNumber = 0;
            SpeedOrg = 0;
            Speed = 0;
            Distance = 0;
            tagStartIndex = 0;
            tagDecelIndex = 0;// 사실상 안쓰임
            tagEndIndex = 0;

            Section_Idx = 0;
            Alloc = false;
            AllocIdx = 0;

            DrivePointList.Clear();
            DriveSegmentList.Clear();
        }

        public object Clone()
        {
            SectionData NewObj = new SectionData
            {
                SteerDirection = this.SteerDirection,
                LinkType = this.LinkType,
                NextSteerDirection = this.NextSteerDirection,

                StartNodeNumber = this.StartNodeNumber,
                EndNodeNumber = this.EndNodeNumber,
                SpeedOrg = this.SpeedOrg,
                Speed = this.Speed,
                Distance = this.Distance,
                tagStartIndex = this.tagStartIndex,
                tagDecelIndex = this.tagDecelIndex,
                tagEndIndex = this.tagEndIndex,

                Section_Idx = this.Section_Idx,
                Alloc = this.Alloc
            };

            NewObj.DrivePointList.AddRange(this.DrivePointList.ToArray());
            NewObj.DriveSegmentList.AddRange(this.DriveSegmentList.ToArray());

            return NewObj;
        }
    }
}
