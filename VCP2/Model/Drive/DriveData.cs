﻿using SSLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.Drive
{
    public class DriveData : ICloneable
    {
        public int StartNodeNumber;
        public int EndNodeNumber;
        public int Distance;
        public int StartTagCnt;
        public int EndTagCnt;
        public int StopOffset;      // 정지 옵셋         

        public StopType StopType = StopType.Normal; // 포트 탐색 정지 여부
        public SteerDir LastSteerDir = 0;

        /// 총섹션개수는 SectionList.Count 로 
        public List<SectionData> SectionList = new List<SectionData>();

        public object Clone()
        {
            DriveData NewObj = new DriveData
            {
                StartNodeNumber = this.StartNodeNumber,
                EndNodeNumber = this.EndNodeNumber,
                Distance = this.Distance,
                StartTagCnt = this.StartTagCnt,
                EndTagCnt = this.EndTagCnt,
                LastSteerDir = this.LastSteerDir,
                StopType = this.StopType,
                StopOffset = this.StopOffset,
                SectionList = SectionList.Select(item => (SectionData)item.Clone()).ToList()
            };

            return NewObj;
        }
    }
}
