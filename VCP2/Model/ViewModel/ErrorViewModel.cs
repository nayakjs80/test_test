﻿using OCS.VCP.Controller;
using OCS.VCP.Model.TagSet;
using SSLib.Extentions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OCS.VCP.Model.ViewModel
{
    class ErrorViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<BitData> _ErrorCollection = null;
        public ObservableCollection<BitData> ErrorCollection
        {
            get { return _ErrorCollection; }
            set { _ErrorCollection = value; RaisePropertyChanged(nameof(ErrorCollection)); }
        }


        private int enumCount;
        private Array enumValue;
        private Timer CollectionUpdateTimer;
        public ErrorViewModel()
        {
            ErrorCollection = new ObservableCollection<BitData>();
            string[] enumName = typeof(VehicleError).GetEnumNames();
            enumCount = enumName.Length;
            enumValue = typeof(VehicleError).GetEnumValues();

            for (int i = 0; i < enumCount; i++)
            {
                ErrorCollection.Add(new BitData() { Name = enumName[i], Index = (int)enumValue.GetValue(i), Visible = true ,Time = null});
            }


            CollectionUpdateTimer = new Timer(1000);
            CollectionUpdateTimer.Elapsed += CollectionUpdateTimer_Elapsed;
            CollectionUpdateTimer.Start();
        }

        private void CollectionUpdateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < enumCount; i++)
            {
                ErrorCollection[i].BitValue = MotionController.Instance.TagStatus_PtoC.ErrorCode.GetBit((int)enumValue.GetValue(i));
                if (ErrorCollection[i].BitValue && ErrorCollection[i].Time == null)
                    ErrorCollection[i].Time = DateTime.Now.ToString("yyyy:MM:dd:HH:mm:ss");
                else if (ErrorCollection[i].BitValue == false)
                    ErrorCollection[i].Time = null;

            }
        }

        #region MVVM 관련 이벤트
        public event PropertyChangedEventHandler PropertyChanged;


        private Action<PropertyChangedEventArgs> RaisePropertyChanged()
        {
            return args => PropertyChanged?.Invoke(this, args);
        }
        private void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool canExecute = true;

        public bool CanExecute
        {
            get
            {
                return this.canExecute;
            }

            set
            {
                if (this.canExecute == value)
                {
                    return;
                }

                this.canExecute = value;
            }
        }
        #endregion
    }
}
