﻿using OCS.VCP.Model.TagSet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using OCS.VCP.Controller;
using SSLib.Extentions;
using System.ComponentModel;

namespace OCS.VCP.Model.ViewModel
{
    class IOViewModel :INotifyPropertyChanged
    {

        #region Singleton
        private static volatile IOViewModel instance;
        private static object syncRoot = new object();

        public static IOViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new IOViewModel();
                    }
                }

                return instance;
            }
        }
        #endregion

        private ObservableCollection<BitData> _IOCollection = null;
        public ObservableCollection<BitData> IOCollection
        {
            get { return _IOCollection; }
            set { _IOCollection = value; RaisePropertyChanged(nameof(IOCollection)); }
        }


        private int enumCount;
        private Array enumValue;
        private Timer CollectionUpdateTimer;
        public IOViewModel()
        {
            IOCollection = new ObservableCollection<BitData>();
            string[] enumName = typeof(VehicleIO).GetEnumNames();
            enumCount = enumName.Length;
            enumValue = typeof(VehicleIO).GetEnumValues();
            
            for (int i = 0; i < enumCount; i++)
            {
                IOCollection.Add(new BitData() { Name = enumName[i], Index = (int)enumValue.GetValue(i), Visible = true });
            }

            CollectionUpdateTimer = new Timer();

            CollectionUpdateTimer = new Timer(1000);
            CollectionUpdateTimer.Elapsed += CollectionUpdateTimer_Elapsed;
            CollectionUpdateTimer.Start();
        }

        private void CollectionUpdateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < enumCount; i++)
            {
                IOCollection[i].BitValue = MotionController.Instance.TagStatus_PtoC.IO.GetBit((int)enumValue.GetValue(i));
            }
        }

        #region MVVM 관련 이벤트
        public event PropertyChangedEventHandler PropertyChanged;


        private Action<PropertyChangedEventArgs> RaisePropertyChanged()
        {
            return args => PropertyChanged?.Invoke(this, args);
        }
        private void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool canExecute = true;

        public bool CanExecute
        {
            get
            {
                return this.canExecute;
            }

            set
            {
                if (this.canExecute == value)
                {
                    return;
                }

                this.canExecute = value;
            }
        }
        #endregion
    }
}
