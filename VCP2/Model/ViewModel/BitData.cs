﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Model.ViewModel
{
    class BitData : INotifyPropertyChanged
    {
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set
            {
                if (_Name != value)
                    _Name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        private int _Index;

        public int Index
        {
            get { return _Index; }
            set
            {
                if (_Index != value)
                    _Index = value;
                RaisePropertyChanged(nameof(Index));
            }
        }

        private bool _BitValue;

        public bool BitValue
        {
            get { return _BitValue; }
            set { if (_BitValue != value)
                    _BitValue = value;
                RaisePropertyChanged(nameof(BitValue));
            }
        }
        private bool _Visible;

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (_Visible != value)
                    _Visible = value;
                RaisePropertyChanged(nameof(Visible));
            }
        }
        private string _Time;

        public string Time
        {
            get { return _Time; }
            set
            {
                if (_Time != value)
                    _Time = value;
                RaisePropertyChanged(nameof(Time));
            }
        }



        #region MVVM 관련 이벤트
        public event PropertyChangedEventHandler PropertyChanged;


        private Action<PropertyChangedEventArgs> RaisePropertyChanged()
        {
            return args => PropertyChanged?.Invoke(this, args);
        }
        private void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool canExecute = true;

        public bool CanExecute
        {
            get
            {
                return this.canExecute;
            }

            set
            {
                if (this.canExecute == value)
                {
                    return;
                }

                this.canExecute = value;
            }
        }
        #endregion
    }
}
