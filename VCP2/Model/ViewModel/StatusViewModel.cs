﻿using OCS.VCP.Model.TagSet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using OCS.VCP.Controller;
using SSLib.Extentions;
using System.ComponentModel;

namespace OCS.VCP.Model.ViewModel
{
    class StatusViewModel :INotifyPropertyChanged
    {

        #region Singleton
        private static volatile StatusViewModel instance;
        private static object syncRoot = new object();

        public static StatusViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new StatusViewModel();
                    }
                }

                return instance;
            }
        }
        #endregion


        private short _BeforeNode;
        public short BeforeNode
        {
            get
            {
                return _BeforeNode;
            }
            set
            {
                _BeforeNode = value;
                RaisePropertyChanged(nameof(BeforeNode));
            }
        }

        private short _CurrentNode;
        public short CurrentNode
        {
            get
            {
                return _CurrentNode;
            }
            set
            {
                _CurrentNode = value;
                RaisePropertyChanged(nameof(CurrentNode));
            }
        }

        private int _Speed;
        public int Speed
        {
            get
            {
                return _Speed;
            }
            set
            {
                _Speed = value;
                RaisePropertyChanged(nameof(Speed));
            }
        }

        private int _Drive_Distance;
        public int Drive_Distance
        {
            get
            {
                return _Drive_Distance;
            }
            set
            {
                _Drive_Distance = value;
                RaisePropertyChanged(nameof(Drive_Distance));
            }
        }

        private int _Previous_Drive_Distance;
        public int Previous_Drive_Distance
        {
            get
            {
                return _Previous_Drive_Distance;
            }
            set
            {
                _Previous_Drive_Distance = value;
                RaisePropertyChanged(nameof(Previous_Drive_Distance));
            }
        }


        #region CPS

        private float _CPS_Status;
        public float CPS_Status
        {
            get
            {
                return _CPS_Status;
            }
            set
            {
                _CPS_Status = value;
                RaisePropertyChanged(nameof(CPS_Status));
            }
        }

        private float _CPS_Regulator_Input_Voltage1;
        public float CPS_Regulator_Input_Voltage1
        {
            get
            {
                return _CPS_Regulator_Input_Voltage1;
            }
            set
            {
                _CPS_Regulator_Input_Voltage1 = value;
                RaisePropertyChanged(nameof(CPS_Regulator_Input_Voltage1));
            }
        }

        private float _CPS_Regulator_Input_Voltage2;
        public float CPS_Regulator_Input_Voltage2
        {
            get
            {
                return _CPS_Regulator_Input_Voltage2;
            }
            set
            {
                _CPS_Regulator_Input_Voltage2 = value;
                RaisePropertyChanged(nameof(CPS_Regulator_Input_Voltage2));
            }
        }

        private float _CPS_Regulator_Output_Voltage;
        public float CPS_Regulator_Output_Voltage
        {
            get
            {
                return _CPS_Regulator_Output_Voltage;
            }
            set
            {
                _CPS_Regulator_Output_Voltage = value;
                RaisePropertyChanged(nameof(CPS_Regulator_Output_Voltage));
            }
        }

        private float _CPS_Regulator_Output_Ampare_1;
        public float CPS_Regulator_Output_Ampare_1
        {
            get
            {
                return _CPS_Regulator_Output_Ampare_1;
            }
            set
            {
                _CPS_Regulator_Output_Ampare_1 = value;
                RaisePropertyChanged(nameof(CPS_Regulator_Output_Ampare_1));
            }
        }

        private float _CPS_Regulator_Output_Ampare_2;
        public float CPS_Regulator_Output_Ampare_2
        {
            get
            {
                return _CPS_Regulator_Output_Ampare_2;
            }
            set
            {
                _CPS_Regulator_Output_Ampare_2 = value;
                RaisePropertyChanged(nameof(CPS_Regulator_Output_Ampare_2));
            }
        }

        private float _CPS_Regulator_Inside_Temp;
        public float CPS_Regulator_Inside_Temp
        {
            get
            {
                return _CPS_Regulator_Inside_Temp;
            }
            set
            {
                _CPS_Regulator_Inside_Temp = value;
                RaisePropertyChanged(nameof(CPS_Regulator_Inside_Temp));
            }
        }

        private float _CPS_ECO_Ampare;
        public float CPS_ECO_Ampare
        {
            get
            {
                return _CPS_ECO_Ampare;
            }
            set
            {
                _CPS_ECO_Ampare = value;
                RaisePropertyChanged(nameof(CPS_ECO_Ampare));
            }
        }

        private float _CPS_REG_ECO_Total_Ampare;
        public float CPS_REG_ECO_Total_Ampare
        {
            get
            {
                return _CPS_REG_ECO_Total_Ampare;
            }
            set
            {
                _CPS_REG_ECO_Total_Ampare = value;
                RaisePropertyChanged(nameof(CPS_REG_ECO_Total_Ampare));
            }
        }

        private float _CPS_ECO_Inside_Temp;
        public float CPS_ECO_Inside_Temp
        {
            get
            {
                return _CPS_ECO_Inside_Temp;
            }
            set
            {
                _CPS_ECO_Inside_Temp = value;
                RaisePropertyChanged(nameof(CPS_ECO_Inside_Temp));
            }
        }

        private float _CPS_PickUp_1_Temp;
        public float CPS_PickUp_1_Temp
        {
            get
            {
                return _CPS_PickUp_1_Temp;
            }
            set
            {
                _CPS_PickUp_1_Temp = value;
                RaisePropertyChanged(nameof(CPS_PickUp_1_Temp));
            }
        }

        private float _CPS_PickUp_2_Temp;
        public float CPS_PickUp_2_Temp
        {
            get
            {
                return _CPS_PickUp_2_Temp;
            }
            set
            {
                _CPS_PickUp_2_Temp = value;
                RaisePropertyChanged(nameof(CPS_PickUp_2_Temp));
            }
        }
        
        private float _CPS_Error_Code;
        public float CPS_Error_Code
        {
            get
            {
                return _CPS_Error_Code;
            }
            set
            {
                _CPS_Error_Code = value;
                RaisePropertyChanged(nameof(CPS_Error_Code));
            }
        }
        #endregion



        private Timer UpdateTimer;
        private StatusViewModel()
        {
            UpdateTimer = new Timer(1000);
            UpdateTimer.Elapsed += CollectionUpdateTimer_Elapsed;
            UpdateTimer.Start();
        }

        private void CollectionUpdateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            BeforeNode = CoreController.Instance.BeforeNode;
            CurrentNode = CoreController.Instance.CurrentNode;

            Previous_Drive_Distance = CoreController.Instance.Previous_Distance;
            Drive_Distance = CoreController.Instance.Distance;

            Speed = CoreController.Instance.Speed;

            CPS_Status = CoreController.Instance.CPS_Status;
            CPS_Regulator_Input_Voltage1 = CoreController.Instance.CPS_Regulator_Input_Voltage1;
            CPS_Regulator_Input_Voltage2 = CoreController.Instance.CPS_Regulator_Input_Voltage2;
            CPS_Regulator_Output_Voltage = CoreController.Instance.CPS_Regulator_Output_Voltage;
            CPS_Regulator_Output_Ampare_1 = CoreController.Instance.CPS_Regulator_Output_Ampare_1;
            CPS_Regulator_Output_Ampare_2 = CoreController.Instance.CPS_Regulator_Output_Ampare_2;
            CPS_Regulator_Inside_Temp = CoreController.Instance.CPS_Regulator_Inside_Temp;
            CPS_ECO_Ampare = CoreController.Instance.CPS_ECO_Ampare;
            CPS_REG_ECO_Total_Ampare = CoreController.Instance.CPS_REG_ECO_Total_Ampare;
            CPS_ECO_Inside_Temp = CoreController.Instance.CPS_ECO_Inside_Temp;
            CPS_PickUp_1_Temp = CoreController.Instance.CPS_PickUp_1_Temp;
            CPS_PickUp_2_Temp = CoreController.Instance.CPS_PickUp_2_Temp;
            CPS_Error_Code = CoreController.Instance.CPS_Error_Code;

        }

        #region MVVM 관련 이벤트
        public event PropertyChangedEventHandler PropertyChanged;


        private Action<PropertyChangedEventArgs> RaisePropertyChanged()
        {
            return args => PropertyChanged?.Invoke(this, args);
        }
        private void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool canExecute = true;

        public bool CanExecute
        {
            get
            {
                return this.canExecute;
            }

            set
            {
                if (this.canExecute == value)
                {
                    return;
                }

                this.canExecute = value;
            }
        }
        #endregion
    }
}
