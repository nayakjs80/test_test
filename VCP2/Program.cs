﻿using OCS.VCP.Config;
using SSLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OCS.VCP
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            try
            {
                string mutexName = "SSFA_VCP2_OHT";
                bool enableCreate = false;
                Mutex mutex = new Mutex(true, mutexName, out enableCreate);

                if (!enableCreate)
                {
                    MessageBox.Show("Program Already Running.");
                    return;
                }

                SSLogManager.Instance.Initialize(new SSLogConfig()
                {
                    AppName = $"VCP2_{ConfigData.Instance.VehicleNumber}",
                    DefaultPath = @"c:\SS_OCS",
                    RemainDays = 60
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
