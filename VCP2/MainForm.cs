﻿using OCS.VCP.Config;
using OCS.VCP.Controller;
using OCS.VCP.View;
using OCSProtocol.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace OCS.VCP
{
    public partial class MainForm : Form
    {
       
        public MainForm()
        {
            InitializeComponent();
            InitializeLayout();
            InitializeProgramStatusPanel();
            Text = $"VCP_{ConfigData.Instance.VehicleNumber}";

        }

        private void InitializeProgramStatusPanel()
        {
            spPlcActive.Active = false;
            spCIMConnect.Active = false;
            spMode.Active = false;
            spState.Active = false;
            spSoonArrive.Active = false;
            spCSTContain.Active = false;
            spCmdEnable.Active = false;

            spPlcActive.Title = "PLC Disconnected";
            spCIMConnect.Title = "CIM DisConnected";
            spMode.Title = "Mode NONE";
            spState.Title = "State NONE";
            spCSTContain.Title = "CST Contain";
            spSoonArrive.Title = "Soon Arrive";
            spCmdEnable.Title = "Command Enable";
        }

        private void InitializeLayout()
        {
            dockPanel.Theme = new VS2015LightTheme();

            DockController.Instance.PLC.Show(dockPanel, DockState.DockLeft);
            DockController.Instance.IO.Show(dockPanel, DockState.DockRight);
            DockController.Instance.Status.Show(dockPanel, DockState.Document);
            DockController.Instance.Error.Show(dockPanel, DockState.DockBottom);

            pLCToolStripMenuItem.Checked = false;
            errorListToolStripMenuItem.Checked = true;
            statusToolStripMenuItem.Checked = true;
            iOToolStripMenuItem.Checked = true;

            DockController.Instance.PLC.SetVisible(pLCToolStripMenuItem.Checked);
            DockController.Instance.Status.SetVisible(statusToolStripMenuItem.Checked);
            DockController.Instance.IO.SetVisible(iOToolStripMenuItem.Checked);
            DockController.Instance.Error.SetVisible(errorListToolStripMenuItem.Checked);

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            SearchHelper.Instance.Initializes();
            ComController.Instance.Initializes();
            MotionController.Instance.Initializes();
            CoreController.Instance.Initializes();
            DockController.Instance.Status.Initializes();
        }

        #region Menu Handler

        private void PLCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pLCToolStripMenuItem.Checked = !pLCToolStripMenuItem.Checked;
            DockController.Instance.PLC.SetVisible(pLCToolStripMenuItem.Checked);
        }

        private void StatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusToolStripMenuItem.Checked = !statusToolStripMenuItem.Checked;
            DockController.Instance.Status.SetVisible(statusToolStripMenuItem.Checked);
        }

        private void iOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            iOToolStripMenuItem.Checked = !iOToolStripMenuItem.Checked;
            DockController.Instance.IO.SetVisible(iOToolStripMenuItem.Checked);
        }

        private void errorListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            errorListToolStripMenuItem.Checked = !errorListToolStripMenuItem.Checked;
            DockController.Instance.Error.SetVisible(errorListToolStripMenuItem.Checked);
        }
        #endregion



        private new void Update()
        {
            spCSTContain.Active = CoreController.Instance.CST_Contain;
            spSoonArrive.Active = CoreController.Instance.SoonArrive;
            spCmdEnable.Active = CoreController.Instance.CMD_Enable;

            if (MotionController.Instance.PLCActiveState)
            {
                spPlcActive.Active = true;
                spPlcActive.Title = "PLC Connected";

                spState.Title = CoreController.Instance.VehicleState.ToString();
                spState.ForeColor = Color.Black;

                switch (CoreController.Instance.VehicleState)//UI 비클과 동일하게 하면 될듯
                {
                    case VEHICLE_STATE.NONE:
                        spState.BackColor = Color.DarkGray;
                        break;
                    case VEHICLE_STATE.INITIALIZING:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.INITIALIZED:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.GO:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.ARRIVE:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.LOADING:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.LOADED:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.UNLOADING:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.UNLOADED:
                        spState.BackColor = Color.GreenYellow;
                        break;
                    case VEHICLE_STATE.CANCEL:
                        break;
                    case VEHICLE_STATE.ERROR:
                        spState.BackColor = Color.Red;
                        spState.ForeColor = Color.White;
                        break;
                    default:
                        break;
                }

                spMode.Title = CoreController.Instance.Mode.ToString();
                switch (CoreController.Instance.Mode)
                {
                    case VEHICLE_MODE.NONE:
                        spMode.BackColor = Color.DarkGray;
                        break;
                    case VEHICLE_MODE.MANUAL:
                        spMode.BackColor = Color.DodgerBlue;
                        break;
                    case VEHICLE_MODE.AUTO:
                        spMode.BackColor = Color.GreenYellow;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                spCSTContain.Active = false;
                spSoonArrive.Active = false;
                spCmdEnable.Active = false;

                spMode.Active = false;
                spPlcActive.Active = false;
                spState.Active = false;
                spPlcActive.Title = "PLC Disconnected";
                spMode.Title = "Mode NONE";
                spState.Title = "State NONE";
            }

            if (ComController.Instance.IsCoreConnected)
            {
                spCIMConnect.Active = true;
                spCIMConnect.Title = "CIM Connected";
            }
            else
            {
                spCIMConnect.Active = false;
                spCIMConnect.Title = "CIM DisConnected";
            }

            spPlcActive.Invalidate();
            spCIMConnect.Invalidate();
            spMode.Invalidate();
            spState.Invalidate();
            spSoonArrive.Invalidate();
            spCSTContain.Invalidate();
            spCmdEnable.Invalidate();

        }

        private void VCPStatusTimer_Tick(object sender, EventArgs e)
        {
            Update();
        }

        private void estopButton_Click(object sender, EventArgs e)
        {
            MotionController.Instance.EStop();
        }

    }
}
