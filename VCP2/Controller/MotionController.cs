﻿using OCS.VCP.Model.TagSet;
using OCSProtocol.Model;
using OMRON.Compolet.Variable;
using SSDatabase.Models.RunData;
using SSLib.Extentions;
using SSLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OCS.VCP.Controller
{
    public partial class MotionController
    {

        #region Singleton
        private static volatile MotionController instance;
        private static object syncRoot = new object();

        public static MotionController Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new MotionController();
                    }
                }

                return instance;
            }
        }
        #endregion

        #region Field
        private object LockOperation_CtoP = new object();
        private object LockCommand_CtoP = new object();
        private object LockSection_CtoP = new object();
        private object LockObject_PtoC = new object();

        private FindPathHelper findPathHelper;

        public bool ConnectionState { get; set; }
        public bool PLCActiveState { get { return ReadCompolet.Active && WriteComolet.Active; }}

        private VariableCompolet ReadCompolet;
        private VariableCompolet WriteComolet;

        private ushort Heartbit_CtoP;
        private ushort Heartbit_PtoC;
        private ushort Heartbit_PtoC_Before;

        public Tag_Command TagCommand_CtoP;
        public Tag_Command TagCommand_PtoC;

        public Tag_Operation_CIM TagOperation_CtoP;
        public Tag_Operation_PLC TagOperation_PtoC;
        public Tag_Status TagStatus_PtoC;

        private readonly int SectionMax = 5;
        private readonly int SectionSize = 96;
        public List<Tag_Section> TagSectionList_CtoP;
        public List<Tag_Section> TagSectionList_PtoC;
        #endregion
        
        private MotionController()
        {
            ReadCompolet = new VariableCompolet();
            WriteComolet = new VariableCompolet();

            TagCommand_CtoP = new Tag_Command();
            TagCommand_PtoC = new Tag_Command();

            TagOperation_CtoP = new Tag_Operation_CIM();
            TagOperation_PtoC = new Tag_Operation_PLC();

            TagStatus_PtoC = new Tag_Status();

            TagSectionList_CtoP = new List<Tag_Section>(SectionMax);
            TagSectionList_PtoC = new List<Tag_Section>(SectionMax);

            for (int i = 0; i < SectionMax; i++)
            {
                TagSectionList_CtoP.Add(new Tag_Section());
                TagSectionList_PtoC.Add(new Tag_Section());
            }
        }

        public void Initializes()
        {
            ReadCompolet.Active = true;
            WriteComolet.Active = true;

            findPathHelper = new FindPathHelper();

            Task.Run(() => Read());
            Task.Run(() => Heartbit());
        }

        private void Read()
        {
            while (true)
            {
                try
                {
                    Read_CIMToPLC();
                    Read_PLCToCIM();
                }
                catch (Exception ex)
                {
                    SSLogManager.Instance.Exception(ex.Message + ex.StackTrace);
                }
                finally
                {
                    Thread.Sleep(1);
                }
            }
        }

  

        private void Heartbit()
        {
            Stopwatch swPtoC = new Stopwatch();
            Stopwatch swCtoP = new Stopwatch();
            TimeSpan timeover = TimeSpan.FromSeconds(10);
            TimeSpan period = TimeSpan.FromSeconds(1);

            ushort min = 1;
            ushort max = 1000;

            swCtoP.Restart();
            swPtoC.Restart();

            while (true)
            {
                #region PLC Hearbit check. Not changed duaring 10sec, it is time over.
                if (Heartbit_PtoC_Before != Heartbit_PtoC)
                {
                    swPtoC.Restart();
                    Heartbit_PtoC_Before = Heartbit_PtoC;
                }

                if (swPtoC.Elapsed > timeover)
                    ConnectionState = false;
                else
                    ConnectionState = true;
                #endregion

                #region CIM Hearbit check. Increase heartbit every 1sec.
                if (swCtoP.Elapsed >= period)
                {
                    swCtoP.Restart();

                    if (Heartbit_CtoP >= max)
                        Heartbit_CtoP = min;

                    Heartbit_CtoP++;
                }
                #endregion

                WriteComolet.WriteVariable("PC_HeartBit", Heartbit_CtoP);
                Thread.Sleep(250);
            }
        }

        #region Read
        private void Read_PLCToCIM()
        {
            Heartbit_PtoC = (ushort)(int)ReadCompolet.ReadVariable("Vcle_HeartBit");
            TagStatus_PtoC.Deserialize((byte[])ReadCompolet.ReadVariable("Vcle_To_IPC_Status"));
            TagOperation_PtoC.Deserialize((byte[])ReadCompolet.ReadVariable("Vcle_To_IPC_Opr"));
            TagCommand_PtoC.Deserialize((byte[])ReadCompolet.ReadVariable("Vcle_To_IPC_Cmd"));
            var read_SectionPLC = (byte[])ReadCompolet.ReadVariable("Vcle_To_IPC_Section");

            for (int i = 0; i < SectionMax; i++)
            {
                var sectionPLC = TagSectionList_PtoC[i];
                sectionPLC.Deserialize(read_SectionPLC.Skip(SectionSize * i).Take(SectionSize).ToArray());
                TagSectionList_PtoC[i] = sectionPLC;
            }
        }

        private void Read_CIMToPLC()
        {
            Heartbit_CtoP = (ushort)(int)ReadCompolet.ReadVariable("PC_HeartBit");

            lock (LockOperation_CtoP)
            {
                TagOperation_CtoP.Deserialize((byte[])ReadCompolet.ReadVariable("IPC_To_Vcle_Opr"));
            }
            //lock (LockCommand_CtoP)
            //{
            //    TagCommand_CtoP.Deserialize((byte[])ReadCompolet.ReadVariable("IPC_To_Vcle_Cmd"));
            //}
            lock (LockSection_CtoP)
            {
                var read_SectionCIM = (byte[])ReadCompolet.ReadVariable("IPC_To_Vcle_Section");

                for (int i = 0; i < SectionMax; i++)
                {
                    var sectionCIM = TagSectionList_CtoP[i];
                    sectionCIM.Deserialize(read_SectionCIM.Skip(SectionSize * i).Take(SectionSize).ToArray());
                    TagSectionList_CtoP[i] = sectionCIM;
                }
            }
        }
        #endregion

        #region Write

        public void Write_IPC_Section()
        {
            lock (LockSection_CtoP)
            {
                using (MemoryStream memStream = new MemoryStream())
                {
                    for (int i = 0; i < SectionMax; i++)
                        memStream.Write(TagSectionList_CtoP[i].Serialize(), 0, SectionSize);

                    byte[] listBytes = memStream.ToArray();
                    WriteComolet.WriteVariable("IPC_To_Vcle_Section", listBytes);
                }
            }
        }

        public void Write_IPC_Command()
        {
            WriteComolet.WriteVariable("IPC_To_Vcle_CMD", TagCommand_CtoP.Serialize());
        }

        public void Write_IPC_Command(CommandCode commandCode)
        {
            lock (LockCommand_CtoP)
            {
                TagCommand_CtoP.Cmd_Number = (short)commandCode;
                WriteComolet.WriteVariable("IPC_To_Vcle_CMD", TagCommand_CtoP.Serialize());
            }
        }

        public void Write_IPC_Operation()
        {
            lock (LockOperation_CtoP)
            {
                WriteComolet.WriteVariable("IPC_To_Vcle_Opr", TagOperation_CtoP.Serialize());
            }
        }

        public void Write_IPC_Operation(OperationCIM operation, bool value)
        {
            lock (LockOperation_CtoP)
            {
                TagOperation_CtoP.Operation.SetBit((int)operation, value);
                WriteComolet.WriteVariable("IPC_To_Vcle_Opr", TagOperation_CtoP.Serialize());
            }
        }

        // Simulation용도. PLC에서 CIM으로 보내는 영역을 CIM이 Write할일은 없다.
        private void Write_PLCToCIM()
        {
            ReadCompolet.WriteVariable("Vcle_To_IPC_Status", TagStatus_PtoC.Serialize());
            ReadCompolet.WriteVariable("Vcle_To_IPC_CMD", TagCommand_PtoC.Serialize());
            WriteComolet.WriteVariable("Vcle_To_IPC_Opr", TagOperation_PtoC.Serialize());

            using (MemoryStream memStream = new MemoryStream())
            {
                for (int i = 0; i < SectionMax; i++)
                    memStream.Write(TagSectionList_PtoC[i].Serialize(), 0, 48 * 2);

                byte[] listBytes = memStream.ToArray();
                ReadCompolet.WriteVariable("Vcle_To_IPC_Section", listBytes);
            }
        }
        #endregion

        public bool GetOperationPtoC(OperationPLC index)
        {
            lock (LockObject_PtoC)
            {
                return TagOperation_PtoC.Operation.GetBit((int)index);
            }
        }
        public bool GetOperationCtoP(OperationCIM index)
        {
            lock (LockOperation_CtoP)
            {
                return TagOperation_CtoP.Operation.GetBit((int)index);
            }
        }
        /// Cim To PLC 전체 초기화
        private void Initialize_Tag_CtoP()
        {
            lock (LockOperation_CtoP)
            {
                TagOperation_CtoP.Operation.Initialize();
            }
            lock (LockCommand_CtoP)
            {
                TagCommand_CtoP.Initialize();
            }
            lock (LockSection_CtoP)
            {
                for (int i = 0; i < SectionMax; i++)
                {
                    TagSectionList_CtoP[i].Initialize();
                }
            }
        }
    }
}
