﻿using OCS.VCP.Config;
using OCS.VCP.Controller;
using OCS.VCP.Model.Drive;
using OCS.VCP.Model.TagSet;
using OCSProtocol.Model;
using SSDatabase.Models.RunData;
using SSLib;
using SSLib.Extentions;
using SSLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OCS.VCP
{
    class CoreController  // Status 관련 처리 및 주행할 SectionData를 가지고 있는 중요 클래스
    {
        #region Singleton
        private static volatile CoreController instance;

        public static CoreController Instance
        {
            get
            {
                if (instance == null)
                    instance = new CoreController();
                return instance;
            }
        }

        private CoreController()
        {
        }
        #endregion

        #region Field
        public object LockDriveData = new object();
        private object LockBCR = new object();
        private object LockEndPoint = new object();

        private string previousDirectionLog = string.Empty;
        private string previousPositionLog = string.Empty;
        private string previousEndPointEmptyLog = string.Empty;

        private Tag_Status currentStutus;
        //private Tag_Status previousStutus;

        private int errorCount;
        private int _LastEndPointNumber;

        private MotionController motion;
        private SearchHelper search;

        #endregion

        #region Property


        //////////////////////////////////////////////// Status 관련 ////////////////////////////////////////////////
        #region Core에 Notify로 전달되는 Property
        public bool CST_Contain { get; private set; }
        public bool SoonArrive { get; private set; }
        public bool CMD_Enable { get; private set; }
        public string CST_ID { get; private set; }
        public int Distance { get; private set; }
        public int Previous_Distance { get; private set; }
        public bool IsError { get; private set; }
        public bool IO_3 { get; private set; }
        public bool IO_2 { get; private set; }
        public bool IO_1 { get; private set; }
        public VEHICLE_MODE Mode { get; private set; }
        public short[] ErrorCodeArray { get; private set; } = new short[10];
        public short BeforeNode { get; private set; }
        public short CurrentNode { get; private set; }
        public int Speed { get; private set; }
        #endregion

        #region CPS Property
        public float CPS_Status { get; private set; }
        public float CPS_Regulator_Input_Voltage1 { get; private set; }
        public float CPS_Regulator_Input_Voltage2 { get; private set; }
        public float CPS_Regulator_Output_Voltage { get; private set; }
        public float CPS_Regulator_Output_Ampare_1 { get; private set; }
        public float CPS_Regulator_Output_Ampare_2 { get; private set; }
        public float CPS_Regulator_Inside_Temp { get; private set; }
        public float CPS_ECO_Ampare { get; private set; }
        public float CPS_REG_ECO_Total_Ampare { get; private set; }
        public float CPS_ECO_Inside_Temp { get; private set; }
        public float CPS_PickUp_1_Temp { get; private set; }
        public float CPS_PickUp_2_Temp { get; private set; }
        public float CPS_Error_Code { get; private set; }
        #endregion

        #region Position 관련
        public SteerDir CurrentGuideDirection { get; private set; }
        public VEHICLE_STATE VehicleState { get; private set; }
        public int DestPointNumber { get; private set; }
        public int LastStartPointNumber { get; private set; }
        public int LastEndPointNumber
        {
            get { lock (LockEndPoint) { return _LastEndPointNumber; } }
            set
            {
                lock (LockEndPoint)
                {
                    if (_LastEndPointNumber != value)
                    {
                        _LastEndPointNumber = value;
                        if (_LastEndPointNumber != 0)
                        {
                            CurrentNode = (short)_LastEndPointNumber;
                            ConfigData.Instance.LastStartPoint = LastStartPointNumber;
                            ConfigData.Instance.LastEndPoint = _LastEndPointNumber;
                            ConfigData.Save();
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        public void Initializes()
        {
            motion = MotionController.Instance;
            search = SearchHelper.Instance;

            Task.Factory.StartNew(CoreThread);
        }

        private void CoreThread()
        {
            SSLogManager.Instance.Write(LogType.System, LogLevel.INFO, "[Core] Core Thread Start.");
            while (true)
            {
                try
                {
                    currentStutus = (Tag_Status)MotionController.Instance.TagStatus_PtoC.Clone();
                    if(CheckIsNull())
                    {
                        UpdateErrorCodeArray();
                        UpdateVehicleState();
                        UpdateDirection();
                        UpdateCPSInfo();
                        UpdateRequestBit();
                        UpdatePositionInfo();
                        UpdateNotifyData();

                        VehicleDrive();
                    }

                    Thread.Sleep(200);
                }
                catch (Exception ex)
                {
                    SSLogManager.Instance.Exception("[Core] Core Thread End. /[Excepion Content] : " + ex.Message + ex.StackTrace);
                    throw ex;
                }
            }
        }

        private void UpdateCPSInfo()
        {
            #region CPS Regulator
            CPS_Status = currentStutus.CPS_Status;
            CPS_Regulator_Input_Voltage1 = currentStutus.CPS_Regulator_Input_Voltage1;
            CPS_Regulator_Input_Voltage2 = currentStutus.CPS_Regulator_Input_Voltage2;
            CPS_Regulator_Output_Voltage = currentStutus.CPS_Regulator_Output_Voltage;
            CPS_Regulator_Output_Ampare_1 = currentStutus.CPS_Regulator_Output_Ampare_1;
            CPS_Regulator_Output_Ampare_2 = currentStutus.CPS_Regulator_Output_Ampare_2;
            CPS_Regulator_Inside_Temp = currentStutus.CPS_Regulator_Inside_Temp;
            CPS_ECO_Ampare = currentStutus.CPS_ECO_Ampare;
            CPS_REG_ECO_Total_Ampare = currentStutus.CPS_REG_ECO_Total_Ampare;
            CPS_ECO_Inside_Temp = currentStutus.CPS_ECO_Inside_Temp;
            CPS_PickUp_1_Temp = currentStutus.CPS_PickUp_1_Temp;
            CPS_PickUp_2_Temp = currentStutus.CPS_PickUp_2_Temp;
            CPS_Error_Code = currentStutus.CPS_Error_Code;
            #endregion
        }

        private bool CheckIsNull()
        {
            bool isNotNull = false;
            if (currentStutus.CstId != null && currentStutus.ErrorCode != null && currentStutus.IO != null && currentStutus.ExtraStatus != null)
                isNotNull = true;
            else
                isNotNull = false;

            if (motion.TagOperation_CtoP.Operation != null && motion.TagOperation_PtoC.Operation != null)
                isNotNull = true;
            else
                isNotNull = false;

            return isNotNull;
        }

        private void ClearCommand()
        {
            if (motion.TagCommand_PtoC.Cmd_Code > 0)
            {
                bool ClearCurrCmd = false;
                if (motion.TagCommand_CtoP.Cmd_Code == motion.TagCommand_PtoC.Cmd_Code)
                {
                    if (motion.TagCommand_CtoP.Cmd_Code == (short)CommandCode.Go && motion.TagCommand_CtoP.Cmd_Code == (short)CommandCode.GoMore)
                    {
                        if (motion.TagCommand_CtoP.Total_Distance == motion.TagCommand_PtoC.Total_Distance)
                            ClearCurrCmd = true;
                    }
                    else
                    {
                        ClearCurrCmd = true;
                    }
                }

                if (motion.TagCommand_CtoP.Cmd_Code == (short)CommandCode.Load && VehicleState == VEHICLE_STATE.LOADING)
                {
                    ClearCurrCmd = true;
                }

                if (motion.TagCommand_CtoP.Cmd_Code == (short)CommandCode.Unload && VehicleState == VEHICLE_STATE.UNLOADING)
                {
                    ClearCurrCmd = true;
                }

                if (motion.TagCommand_CtoP.Cmd_Code == (short)CommandCode.Init && VehicleState == VEHICLE_STATE.INITIALIZED)
                {
                    ClearCurrCmd = true;
                }

                if (motion.TagCommand_CtoP.Cmd_Code == (short)CommandCode.Init && currentStutus.Vcle_Mode == (short)VEHICLE_MODE.AUTO)
                {
                    ClearCurrCmd = true;
                }

                if (motion.TagCommand_CtoP.Cmd_Code == (short)CommandCode.Go && motion.TagCommand_PtoC.Cmd_Code == (short)CommandCode.GoMore)
                {
                    ClearCurrCmd = true;
                }

                /// 커맨드 접수 확인후 커맨드 코드만 클리어. 코드는 클리어하고 기타 정보는 남겨달라한다
                if (ClearCurrCmd)
                {
                    SSLogManager.Instance.Write(SSLog.LogType.System, LogLevel.INFO, string.Format("ClearCmdCode /Code:{0}/Number:{1}/TotalDistance:{2}/PortDir:{3}"
                            , motion.TagCommand_PtoC.Cmd_Code, motion.TagCommand_PtoC.Cmd_Number, motion.TagCommand_PtoC.Total_Distance, motion.TagCommand_PtoC.Direction_Info));
                    
                    motion.TagCommand_CtoP.Cmd_Code = 0;
                    motion.Write_IPC_Command();
                    Thread.Sleep(200);
                }
            }
        }

        private void VehicleDrive()
        {
            if (motion.CheckGoingCommand())
            {
                motion.DriveSection();
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    motion.TagSectionList_CtoP[i].Initialize();
                }
                motion.Write_IPC_Section();
            }
        }

        private void UpdateErrorCodeArray()
        {
            var currentErrorCodeArray = new BitArray(currentStutus.ErrorCode);
            errorCount = 0;
            for (int i = 0; i < ErrorCodeArray.Count(); i++)
            {
                ErrorCodeArray[i] = 0;
            }
            for (short i = 0; i < currentErrorCodeArray.Count; i++)
            {
                if (currentErrorCodeArray[i] == true)
                {
                    ErrorCodeArray[errorCount++] = i;
                }
            }
        }

        private void UpdateVehicleState()
        {
            VEHICLE_STATE currentState = VEHICLE_STATE.NONE;
            switch (currentStutus.Vcle_Status)
            {
                case 100: currentState = VEHICLE_STATE.GO; break;
                case 101: currentState = VEHICLE_STATE.ARRIVE; break;
                case 300: currentState = VEHICLE_STATE.LOADING; break;
                case 301: currentState = VEHICLE_STATE.LOADED; break;
                case 400: currentState = VEHICLE_STATE.UNLOADING; break;
                case 401: currentState = VEHICLE_STATE.UNLOADED; break;
                case 500: currentState = VEHICLE_STATE.INITIALIZING; break;
                case 501: currentState = VEHICLE_STATE.INITIALIZED; break;
                case 999: currentState = VEHICLE_STATE.ERROR; break;
                default:
                    break;
            }
            VehicleState = currentState;
        }

        private void UpdateRequestBit()
        {
            // 리셋 시도 후 에러 상황이 해제 되었다면 리셋 비트를 해제.
            if (motion.GetOperationCtoP(OperationCIM.Err_Reset_Req) && (currentStutus.Vcle_Status != 999))
            {
                motion.Write_IPC_Operation(OperationCIM.Err_Reset_Req, false);
                SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, "[Update Vehicle Status] : FREE ERROR");
            }
        }

        private void UpdateDirection()
        {
            #region Direction
            StringBuilder newDirectionLog = new StringBuilder();

            bool DIRECTION_1_LEFT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_1_LEFT);
            bool DIRECTION_1_RIGHT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_1_RIGHT);
            bool DIRECTION_2_LEFT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_2_LEFT);
            bool DIRECTION_2_RIGHT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_2_RIGHT);
            bool DIRECTION_3_LEFT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_3_LEFT);
            bool DIRECTION_3_RIGHT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_3_RIGHT);
            bool DIRECTION_4_LEFT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_4_LEFT);
            bool DIRECTION_4_RIGHT = currentStutus.IO.GetBit((int)VehicleIO.DIRECTION_4_RIGHT);

            if (DIRECTION_1_LEFT == true && DIRECTION_1_RIGHT == false &&
                DIRECTION_2_LEFT == true && DIRECTION_2_RIGHT == false &&
                DIRECTION_3_LEFT == true && DIRECTION_3_RIGHT == false &&
                DIRECTION_4_LEFT == true && DIRECTION_4_RIGHT == false)
            {
                CurrentGuideDirection = SSLib.SteerDir.Left;
                newDirectionLog.Append($"[Update Vehicle Status] / Direction : Left");
            }
            else if (DIRECTION_1_LEFT == false && DIRECTION_1_RIGHT == true &&
                    DIRECTION_2_LEFT == false && DIRECTION_2_RIGHT == true &&
                    DIRECTION_3_LEFT == false && DIRECTION_3_RIGHT == true &&
                    DIRECTION_4_LEFT == false && DIRECTION_4_RIGHT == true)
            {
                CurrentGuideDirection = SSLib.SteerDir.Right;
                newDirectionLog.Append($"[Update Vehicle Status] / Direction : Right");
            }
            else
            {
                CurrentGuideDirection = SteerDir.None;
                newDirectionLog.Append($"[Update Vehicle Status] Direction:None /");
                newDirectionLog.Append($" Left#1:{DIRECTION_1_LEFT}");
                newDirectionLog.Append($" Left#2:{DIRECTION_2_LEFT}");
                newDirectionLog.Append($" Left#3:{DIRECTION_3_LEFT}");
                newDirectionLog.Append($" Left#4:{DIRECTION_4_LEFT}");
                newDirectionLog.Append($" Right#1:{DIRECTION_1_RIGHT}");
                newDirectionLog.Append($" Right#2:{DIRECTION_2_RIGHT}");
                newDirectionLog.Append($" Right#3:{DIRECTION_3_RIGHT}");
                newDirectionLog.Append($" Right#4:{DIRECTION_4_RIGHT}");
            }

            if (!previousDirectionLog.Equals(newDirectionLog.ToString()))
            {
                previousDirectionLog = newDirectionLog.ToString();
                SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, previousDirectionLog);
            }
            #endregion
        }

        private void UpdateNotifyData()
        {
            Speed = motion.CurrentSpeed;
            CST_Contain = Convert.ToBoolean(currentStutus.ExtraStatus.GetBit(0));
            SoonArrive = Convert.ToBoolean(currentStutus.ExtraStatus.GetBit(1));
            CMD_Enable =  Convert.ToBoolean(currentStutus.CMD_Enable);
            CST_ID = Encoding.Default.GetString(currentStutus.CstId);
            Distance = currentStutus.Drive_Distance;
            Previous_Distance = currentStutus.Previous_Distance;
            IO_1 = false; //TODO    
            IO_2 = false;
            IO_3 = false;
            IsError = (errorCount > 0) ? true : false;
            BeforeNode = currentStutus.Before_Node;
            CurrentNode = currentStutus.Current_Node;
            Mode = (VEHICLE_MODE)currentStutus.Vcle_Mode;
        }

        private void UpdatePositionInfo()
        {
            #region 현재 Start, End Node PLC 값 변동시 Log
            StringBuilder newPositionLog = new StringBuilder();
            newPositionLog.Append($"[Update Vehicle Status] / Node Before:{currentStutus.Before_Node}/Current:{currentStutus.Current_Node}");
            if (!previousPositionLog.Equals(newPositionLog.ToString()))
            {
                previousPositionLog = newPositionLog.ToString();
                SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, previousPositionLog);
            }
            #endregion

            #region Point 객체와 PLC에서 읽힌 Node Number로 초기화하는 단계

            //최초 구동시 Start는 0이고 End는 값이 있다 이럴경우 End값으로 둘다 지정(정위치에서 최초 켜짐)
            if (currentStutus.Before_Node == 0 && currentStutus.Current_Node != 0)
            {
                currentStutus.Before_Node = currentStutus.Current_Node;
            }

            int startPointNumber = 0, endPointNumber = 0;

            Point StartPoint = SearchHelper.Instance.GetPointByBCR(currentStutus.Before_Node);
            Point EndPoint = SearchHelper.Instance.GetPointByBCR(currentStutus.Current_Node); //주행중이거나 잘못 정지됐을때 0으로 올라온다.
            Point DestPoint = SearchHelper.Instance.GetPointByBCR(MotionController.Instance.TagCommand_PtoC.Target_Node);

            if (DestPoint == null)
                DestPointNumber = MotionController.Instance.TagCommand_PtoC.Target_Node;
            else
                DestPointNumber = DestPoint.Number;

            if (StartPoint == null)
            {
                startPointNumber = currentStutus.Before_Node;
                StartPoint = SearchHelper.Instance.GetPointByNum(currentStutus.Current_Node);
            }
            else
                startPointNumber = StartPoint.Number;

            if (EndPoint == null)
                EndPoint = SearchHelper.Instance.GetPointByNum(currentStutus.Current_Node);

            if (LastStartPointNumber != startPointNumber)//Before 값이 바꼈을때 지나온 포인트추가하고, Last변경
            {
                lock (LockDriveData)
                {
                    if (motion.PastPointList.LastOrDefault() != startPointNumber)
                        motion.PastPointList.Add(startPointNumber);
                    LastStartPointNumber = startPointNumber;
                }
                SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"[Update Vehicle Status] / Change BeforeNode {startPointNumber}");
            }
            #endregion

            #region EndPoint 값 처리.
            #region 바코드 위치에 비클이 정위치로 정지 또는 지나가 현재 위치 값이 올라온 경우.
            if (EndPoint != null)
            {
                //1. 주행중 End값이 안올라올 경우 주행정보를 검색하여, 다음위치를 올린다.
                //2. 정지중에 End값이 안올라올 경우, 마지막 주행정보를 확인하여 마지막위치가 Start값에 일치하면 End를 Start로 올린다.
                //PLC에선 현재 위치를 올려주며, 현재위치를 벗어나면 0값이 올라온다.
                lock (LockDriveData)
                {
                    if (motion.PastPointList.LastOrDefault() != EndPoint.Number) //주행 포인트 마지막 번호와 현재 EndPointNum이 다르다면 추가한다. (주행중 지나갈때)
                        motion.PastPointList.Add(EndPoint.Number);
                }

                if (LastEndPointNumber != EndPoint.Number)
                {
                    LastEndPointNumber = EndPoint.Number;
                    SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"[Update Vehicle Status] / Change CurrentNode {LastEndPointNumber}");
                }
            }
            #endregion
            #region 주행중이거나 정위치에서 밀렸을 경우 Current_Node PLC값이 0으로 올라옴
            else//EndPoint == null
            {
                #region Log
                StringBuilder newEndPointEmptyLog = new StringBuilder();
                newEndPointEmptyLog.Append($"[Update Vehicle Status] / Empty EndPoint / Auto {MotionController.Instance.GetOperationPtoC(OperationPLC.Auto_Enable)} / State:{VehicleState.ToString()}");
                if (!previousEndPointEmptyLog.Equals(newEndPointEmptyLog.ToString()))
                {
                    previousEndPointEmptyLog = newEndPointEmptyLog.ToString();
                    SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, previousEndPointEmptyLog);
                }
                #endregion
                lock (LockDriveData)
                {
                    #region 경우 1 :  주행하다 ERROR가 났을경우거나, 처음 VCP 구동시
                    if (!motion.GetOperationPtoC(OperationPLC.Auto_Enable) || VehicleState == VEHICLE_STATE.ERROR)
                    {
                        #region 주행하다 멈춤 경우
                        if (null != StartPoint && LastStartPointNumber == currentStutus.Before_Node) //주행하다 멈춤 경우 StartBarCode와 BeforeNode가 같을 것이다.
                        {
                            //매뉴얼 상황에서 움직여 새로운 세그먼트로 넘어간 상황이라면,
                            if (LastEndPointNumber == startPointNumber)
                            {
                                if (!StartPoint.SegOut.Contains(','))//연결된 길이 하나라면 (','를 구분자가 있으면 갈림길)
                                {
                                    endPointNumber = (int)search.GetSegmentByNumber(int.Parse(StartPoint.SegOut)).EndPnt;
                                    SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"[Update Vehicle Status] / Only One Segment nEndPntNum:{endPointNumber}");
                                }
                                else //갈림길이라면
                                {
                                    foreach (Segment Seg in search.GetSegmentListBySegOutString(StartPoint.SegOut))//연결된 길이 여러개라면 스티어 방향으로 유추.
                                    {
                                        if ((int)CurrentGuideDirection == Seg.SteerDir)
                                        {
                                            endPointNumber = (int)Seg.EndPnt;
                                            SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"[Update Vehicle Status] / Equal Direction Segment EndPointNumber :{endPointNumber}/CurrentDirection:{CurrentGuideDirection.ToString()}/SegmentDirection:{Seg.SteerDir}");
                                            break;
                                        }
                                        SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.WARN, $"[Update Vehicle Status] / Cant Find Equal Direction Segment");
                                    }
                                }
                            }
                            else // 매뉴얼, 에러 상황에서 Before_Node 에 별 변화가 없다면 현재 위치 유지.
                            {
                                if (LastEndPointNumber > 0)
                                    endPointNumber = LastEndPointNumber;
                            }
                        }
                        #endregion
                        #region VCP 껐다 킨경우 (ConfigData로 EndPoint 지정)
                        else if (LastStartPointNumber == 0 && currentStutus.Before_Node > 0)
                        {
                            ConfigData.Refresh();
                            if (ConfigData.Instance.LastStartPoint == startPointNumber)
                            {
                                endPointNumber = ConfigData.Instance.LastEndPoint;
                            }
                            else
                                SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"[Update Vehicle Status] / The values of the Config LastStartPoint and the PLC Before_Node do not match.");
                        }
                        #endregion
                        LastEndPointCalculate(StartPoint, endPointNumber);
                        return;
                    }
                    #endregion 

                    #region 경우 2: 오토 도착 상태인데 정위치가 아닐경우
                    if (motion.GetOperationPtoC(OperationPLC.Auto_Enable) && VehicleState == VEHICLE_STATE.ARRIVE)
                    {
                        // 주행정보가 있고, 주행정보의 마지막 EndNode 번호가 현재 읽힌 startPointNumber와 같다면 이전 노드의 값으로 EndPoint 지정
                        if (motion.RemainSectionList.Count > 0 && motion.RemainSectionList.LastOrDefault()?.EndNodeNumber == startPointNumber)
                        {
                            endPointNumber = startPointNumber;
                            LastEndPointCalculate(StartPoint, endPointNumber);
                            return;
                        }
                        else//아직 주행이 없거나 주행 마지막 정보와 이전 노드값이 다르다면
                        {
                            if (LastStartPointNumber == currentStutus.Before_Node) //오토인데 End값이 없는건 살짝 밀린걸로 치겠다.
                            {
                                endPointNumber = LastEndPointNumber;
                            }
                            else//오토인데, 위치값이 바꼈다?!
                            {
                                endPointNumber = startPointNumber;
                            }
                            LastEndPointCalculate(StartPoint, endPointNumber);
                            return;
                        }
                    }
                    #endregion

                    #region 최초주행시 End값이 Start값으로 미리 넘어갔을 경우 대비...
                    if (motion.PastPointList.Count == 0 && LastEndPointNumber != 0)
                    {
                        motion.PastPointList.Add(LastEndPointNumber);
                    }
                    #endregion

                    //lgy 여기 까지 retrun 되지 않지 않았다면 마지막 주행정보를 가지고 비교해 EndPoint를 지정.
                    #region 가진 주행 정보와 지나온 포인트 정보를 대조해 Before값이 있으면, Before다음 값을 Current(Endpnt)값으로 치겠다.
                    int SearchNode = 0;
                    if (motion.PastPointList.Count > 0)
                    {
                        for (int i = 0; i < motion.RemainSectionList.Count; ++i)
                        {
                            for (int SegIndex = 0; SegIndex < motion.RemainSectionList[i].DriveSegmentList.Count; ++SegIndex)
                            {
                                ++SearchNode;
                                if (motion.PastPointList.Count == SearchNode)
                                {
                                    if (motion.PastPointList.LastOrDefault() == motion.RemainSectionList[i].DriveSegmentList[SegIndex].StartPnt)
                                    {
                                        endPointNumber = (int)motion.RemainSectionList[i].DriveSegmentList[SegIndex].EndPnt;
                                    }
                                    else
                                    {
                                        //주행데이터와 위치데이터가 맞지않는다.                                    
                                        //todo 긴급정지해야하나? 
                                        SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.ERROR,
                                        $"Update Vehicle Status / Invalid Drive PastCount:{motion.PastPointList.Count}/PastLastNode:{motion.PastPointList.LastOrDefault()}/SegStart:{motion.RemainSectionList[i].DriveSegmentList[SegIndex].StartPnt}/SegEnd:{motion.RemainSectionList[i].DriveSegmentList[SegIndex].EndPnt}");

                                    }
                                    break;
                                }
                            }
                        }
                    }
                    LastEndPointCalculate(StartPoint, endPointNumber);
                    #endregion
                }
            }
            #endregion

            #endregion
        }

        private void LastEndPointCalculate(Point StartPoint, int endPointNumber)
        {
            #region 아직도 EndPointNumber가 0이라면 마지막 처리
            if (0 == endPointNumber)
            {
                if (StartPoint != null) // 여기서도 StartPnt가 null일 경우 에러가 남.
                {
                    if (!StartPoint.SegOut.Contains(","))//연결된 길이 하나라면
                    {
                        endPointNumber = (int)SearchHelper.Instance.GetSegmentByNumber(int.Parse(StartPoint.SegOut)).EndPnt;
                    }
                    else
                    {
                        foreach (Segment Seg in SearchHelper.Instance.GetSegmentListBySegOutString(StartPoint.SegOut))
                        {
                            if ((int)CurrentGuideDirection == Seg.SteerDir)
                            {
                                endPointNumber = (int)Seg.EndPnt;
                                break;
                            }
                            // 딱히 추측할만한 값이 없다... 걍 첫번째 길로..
                            endPointNumber = (int)SearchHelper.Instance.GetSegmentByNumber(int.Parse(StartPoint.SegOut)).EndPnt;
                        }
                    }
                }
            }
            #endregion
            LastEndPointNumber = endPointNumber;

            //if (CurrentStutus.Current_Node > 0 && ReadingBarcode != CurrentStutus.Current_Node)
            //    ReadingBarcode = CurrentStutus.Current_Node;
        }
    }
}
