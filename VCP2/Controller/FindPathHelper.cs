﻿using SSDatabase.Models.RunData;
using SSLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Controller
{
    class FindPathHelper
    {
        private int PointCount;

        private int[] V;
        private int[] Via;
        private int[] Distance;
        private int[] Path;
        private int[] bestPath;
        private int[,] AdjacentDistanceMetrix;
        //모든 Segment의 Point와 Point마다간의 거리 매트릭트, 값이 있는건 TravalTime, 
        //Segment에 포함되지 않는 index간에는 MaxValue이다.
        //이걸 통해 특정 포인트에서 연결된 인접 포인트간의 거리를 알수 있다.


        private SearchHelper searchHelper;

        public FindPathHelper()
        {
            searchHelper = SearchHelper.Instance;

            PointCount = searchHelper.PointList.Count;

            V = new int[PointCount];
            Distance = new int[PointCount];
            Via = new int[PointCount];
            Path = new int[PointCount];
            bestPath = new int[PointCount];
            AdjacentDistanceMetrix = new int[PointCount, PointCount];
            AdjacentDistanceMetrix.Initialize();

        }

        private void MakeAdjacentMatrix() //** UseUnuse 미반영. TravelTime 과 Weight만 반영
        {
            #region 자기 자신의 대한 경로는 0, 그 외의 값은 무한대로 거리값을 설정
            for (int i = 0; i < PointCount; i++)
            {
                for (int j = 0; j < PointCount; j++)
                {
                    if (i == j)
                    {
                        AdjacentDistanceMetrix[i, j] = 0; 

                    }
                    else
                    {
                        AdjacentDistanceMetrix[i, j] = int.MaxValue;
                    }
                }
            }
            #endregion

            #region 배열에 각 포인트 인덱스 별 배열요소를 기준으로 SegmentLength/(Speed/1000) 인 TravelTime 을 할당한다.
            try
            {
                foreach (Segment segment in searchHelper.SegmentList)
                {
                    Point startPoint = searchHelper.GetPointByNum((int)segment.StartPnt);
                    Point endPoint = searchHelper.GetPointByNum((int)segment.EndPnt);

                    int startPointIndex = searchHelper.PointList.FindIndex(p => p.Number == startPoint.Number);
                    int endPointIndex = searchHelper.PointList.FindIndex(p => p.Number == endPoint.Number);


                    int travelTime = (int)segment.TravelTime;

                    AdjacentDistanceMetrix[startPointIndex - 1, endPointIndex - 1] = travelTime;
                }
            }
            catch (Exception ex)
            {
                SSLogManager.Instance.Exception(ex.Message + ex.StackTrace);
            }
            #endregion
            
        }

        /// <summary>
        /// StartPointIndex에서 EndPointIndex의 Path를 Dijkstra 알고리즘을 통해 PathList에 세팅하는 함수
        /// </summary>
        /// <param name="StartPointIndex">시작 Point의 List Index</param>
        /// <param name="EndPointIndex">끝 Point의 List Index</param>
        /// <param name="PathArray">경로의 Point Number 배열로, Index 0은 시작 Point Number 다음 지점</param>
        /// <param name="PathSize">PathArray의 Path Point 갯수</param>
        /// <returns></returns>
        public int SetPathByDijkstra(int StartPointIndex, int EndPointIndex, ref int[] PathArray, ref int PathSize)
        {
            //Pnt Index : 1 ~ N

            int i = 0; int j = 0; int k = 0; int Min = 0;
            int pointCount = searchHelper.PointList.Count();
            int pointCountInPath = 0;

            try
            {
                for (j = 0; j < pointCount; j++)//+++ 0820 pnts를 1번째부터 채워넣으므로 0에서 1로 변경.
                {
                    V[j] = 0;
                    Via[j] = StartPointIndex;
                    Distance[j] = AdjacentDistanceMetrix[StartPointIndex, j]; //시작 Point에서 그외 Point까지 계산된 Distance 대입
                    Path[j] = 0;
                    bestPath[j] = 0;
                }

                Distance[StartPointIndex] = 0;

                //매트릭스에 가중치값을 더하여 값을 완성하는 부분
                for (i = 0; i < pointCount; i++)//+++ 0820 pnts를 1번째부터 채워넣으므로 0에서 1로 변경.
                {
                    Min = int.MaxValue;

                    for (j = 0; j < pointCount; j++)//+++ 0820 pnts를 1번째부터 채워넣으므로 0에서 1로 변경.
                    {
                        if (V[j] == 0 && Distance[j] < Min)
                        {
                            k = j;//가장 작은 index 번호 
                            Min = Distance[j];
                        }
                    }

                    V[k] = 1;

                    if (Min == int.MaxValue)
                        break;

                    for (j = 0; j < pointCount; j++)//+++ 0820 pnts를 1번째부터 채워넣으므로 0에서 1로 변경.
                    {
                        if (Distance[j] > Distance[k] + AdjacentDistanceMetrix[k, j])
                        {
                            Distance[j] = Distance[k] + AdjacentDistanceMetrix[k, j];
                            Via[j] = k;
                        }
                    }
                }

                if (Distance[EndPointIndex] == int.MaxValue)
                    return int.MaxValue;

                k = EndPointIndex;

                while (true)
                {
                    Path[pointCountInPath++] = k; //도착의 역순으로 저장

                    if (k == StartPointIndex)
                        break;

                    k = Via[k];//목적 지점의 이전 지점
                }

                //m_BestPath에 Path에 저장된 Point들을 순서대로 저장.
                if (pointCountInPath == 1)
                    PathArray[PathSize++] = EndPointIndex + 1;
                else
                {
                    for (i = pointCountInPath - 2; i >= 0; i--)
                        PathArray[PathSize++] = Path[i] + 1;
                }

                return Distance[EndPointIndex];
            }
            catch (Exception ex)
            {
                SSLog.SSLogManager.Instance.Exception(ex.Message + ex.StackTrace);
                throw ex;
            }
        }
    }
}
