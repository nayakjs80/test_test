﻿using OCS.VCP.Model.TagSet;
using OCSProtocol.Model;
using SSDatabase.Models.RunData;
using SSLib.Extentions;
using SSLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OCS.VCP.Model.Drive;
using SSLib;
using OCS.VCP.Config;

namespace OCS.VCP.Controller
{
    public partial class MotionController
    {
        private Task InitCommandTask;

        #region Drive 관련 Property
        public List<int> PastPointList { get; private set; } = new List<int>();  /// 지나온 포인트를 가지는 리스트
        public List<SectionData> RemainSectionList { get; private set; } = new List<SectionData>();  /// 주행할 주행정보를 PLC에 써주기 위한 Section단위 변환 후 보관된 리스트
        public List<DriveData> DriveDataList { get; private set; } = new List<DriveData>();  /// 한 섹션 주행에 대한 전체 정보가 있는 리스트

        public int WriteStartIndex { get; private set; }
        public int WriteEndIndex { get; private set; }
        public int WriteLastIndex { get; private set; }
        public bool NewGoCommand { get; private set; }
        public int CurrentSpeed { get; private set; }
        #endregion

        #region RequestData 

        public RES_TYPE Go(int startPointNum, int endPointNum, MoveCommandType moveCommandType)
        {
            #region 초기화
            SearchHelper search = SearchHelper.Instance;

            List<Point> PathPoint_List = new List<Point>();
            List<Segment> PathSegment_List = new List<Segment>();

            Point StartPoint = search.GetPointByNum(startPointNum);
            Point EndPoint = search.GetPointByNum(endPointNum);

            if (null == StartPoint || null == EndPoint)
            {
                Log(REQ_TYPE.REQ_GO, "[WARN] can not find the starting point and the end point.");
                return RES_TYPE.RES_NACK;
            }

            int startPointIndex = search.GetIndexByPointNumber(StartPoint.Number);
            int endPointIndex = search.GetIndexByPointNumber(EndPoint.Number);
            int PathSize = 0;
            int[] PathPointIndexArray = new int[search.PointList.Count];
            #endregion

            #region 경로 찾기
            findPathHelper.SetPathByDijkstra(startPointIndex, endPointIndex, ref PathPointIndexArray, ref PathSize);

            if (PathSize == 0)//경로가 안 구해진 경우
                return RES_TYPE.RES_NACK;
            #endregion 

            #region PointList, SegmentList 구성 및 Path SectionList 생성
            PathPoint_List.Add(StartPoint);//첫 포인트 보관

            foreach (int indexNum in PathPointIndexArray) //두번째 부터 끝까지 List에 보관
            {
                if (0 != indexNum)
                {
                    Point PathPoint = search.PointList[indexNum];
                    PathPoint_List.Add(PathPoint);
                }
            }

            search.SetSegmentListByPoints(PathPoint_List, ref PathSegment_List); //포인트를 통해 세그먼트를 구한다.

            List<SectionData> PathSectionList = MakeSectionDataList(PathPoint_List, PathSegment_List); //포인트와 세그먼트를 통해 섹션 구성
            #endregion

            #region DriveData 생성 & Set
            DriveData driveData = new DriveData
            {
                SectionList = PathSectionList,
                StartNodeNumber = startPointNum,
                EndNodeNumber = endPointNum
            };
            foreach (var section in PathSectionList)
            {
                driveData.Distance += section.Distance;
            }
            driveData.StartTagCnt = 0;
            var LastSect = driveData.SectionList.LastOrDefault();
            if (LastSect != null)
                driveData.EndTagCnt = LastSect.tagEndIndex;
            driveData.StopOffset = 0;
            #endregion

            #region GO명령시 이전 주행관련 데이터 전체 초기화
            WriteStartIndex = 0;
            WriteEndIndex = 0;
            WriteLastIndex = 0;
            DriveDataList.Clear();
            PastPointList.Clear();
            RemainSectionList.Clear();
            #endregion

            #region 현재까지 생성된 DriveData를 보관
            DriveDataList.Add(driveData);
            #endregion

            #region MaxNode에 맞게 섹션 데이터 리스트 재생성하고 주행할 SectionList에 보관
            List<SectionData> GoSection = MakeMaxNodeSectionDataList(driveData);
            for (int i = 0; i < GoSection.Count; ++i)
            {
                RemainSectionList.Add(GoSection[i]);
            }
            #endregion

            #region 커맨드 세팅후 NewGoCommand 값 변경
            TagCommand_CtoP.Initialize();
            lock (LockCommand_CtoP)
            {
                TagCommand_CtoP.Moving_Type = (short)moveCommandType;
                TagCommand_CtoP.Start_Node = (short)driveData.StartNodeNumber;
                TagCommand_CtoP.Target_Node = (short)driveData.EndNodeNumber;

                NewGoCommand = true;
            }
            #endregion

            #region Log
            string strSection = "";
            foreach (SectionData Section in RemainSectionList)
            {
                strSection += string.Format("Start:{0:D5},End:{1:D5},Link:{2},Steer:{3},Speed:{4:D4},Dist:{5},StartIdx:{6}EndIDx:{7}\r\n"
                                                , Section.StartNodeNumber
                                                , Section.EndNodeNumber
                                                , Section.LinkType
                                                , Section.SteerDirection
                                                , Section.Speed
                                                , Section.Distance
                                                , Section.tagStartIndex
                                                , Section.tagEndIndex
                                                );
            }
            Log(REQ_TYPE.REQ_GO, strSection);
            #endregion
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE GoMore(int endPointNum, MoveCommandType moveCommandType)
        {
            var lastDriveData = DriveDataList.LastOrDefault();
            if(lastDriveData != null)
            {
                #region 초기화
                SearchHelper search = SearchHelper.Instance;

                List<Point> PathPoint_List = new List<Point>();
                List<Segment> PathSegment_List = new List<Segment>();

                Point StartPoint = search.GetPointByNum(lastDriveData.EndNodeNumber); //마지막 주행 데이터의 끝지점부터
                Point EndPoint = search.GetPointByNum(endPointNum);

                if (null == StartPoint || null == EndPoint)
                {
                    Log(REQ_TYPE.REQ_GOMORE, "[WARN] can not find the starting point and the end point.");
                    return RES_TYPE.RES_NACK;
                }

                int startPointIndex = search.GetIndexByPointNumber(StartPoint.Number);
                int endPointIndex = search.GetIndexByPointNumber(EndPoint.Number);
                int PathSize = 0;
                int[] PathPointIndexArray = new int[search.PointList.Count];
                #endregion

                #region 경로 찾기
                findPathHelper.SetPathByDijkstra(startPointIndex, endPointIndex, ref PathPointIndexArray, ref PathSize);
                if (PathSize == 0)//경로가 안 구해진 경우
                    return RES_TYPE.RES_NACK;
                
                #endregion

                #region PointList, SegmentList 구성 및 Path SectionList 생성
                PathPoint_List.Add(StartPoint);//첫 포인트 보관

                foreach (int indexNum in PathPointIndexArray) //두번째 부터 끝까지 List에 보관
                {
                    if (0 != indexNum)
                    {
                        Point PathPoint = search.PointList[indexNum];
                        PathPoint_List.Add(PathPoint);
                    }
                }

                search.SetSegmentListByPoints(PathPoint_List, ref PathSegment_List); //포인트를 통해 세그먼트를 구한다.

                List<SectionData> PathSectionList = MakeSectionDataList(PathPoint_List, PathSegment_List); //포인트와 세그먼트를 통해 섹션 구성
                #endregion

                #region DriveData 생성 & Set
                DriveData driveData = new DriveData
                {
                    SectionList = PathSectionList,
                    StartNodeNumber = lastDriveData.EndNodeNumber,
                    EndNodeNumber = endPointNum
                };
                foreach (var section in PathSectionList)
                {
                    driveData.Distance += section.Distance;
                }
                driveData.StartTagCnt = 0;
                var LastSect = driveData.SectionList.LastOrDefault();
                if (LastSect != null)
                    driveData.EndTagCnt = LastSect.tagEndIndex;
                driveData.StopOffset = 0;
                #endregion

                #region 현재까지 세팅된 DriveData를 보관
                driveData.StartTagCnt += DriveDataList.LastOrDefault().EndTagCnt;
                driveData.EndTagCnt += DriveDataList.LastOrDefault().EndTagCnt;

                DriveDataList.Add(driveData);
                #endregion

                #region MaxNode에 맞게 섹션 데이터 리스트 재생성하고 주행할 SectionList에 보관
                List<SectionData> GoSection = MakeMaxNodeSectionDataList(driveData);
                for (int i = 0; i < GoSection.Count; ++i)
                {
                    RemainSectionList.Add(GoSection[i]);
                }
                #endregion

                #region 커맨드 세팅
                TagCommand_CtoP.Initialize();

                lock (LockCommand_CtoP)
                {
                    TagCommand_CtoP.Moving_Type = (short)moveCommandType;
                    TagCommand_CtoP.Start_Node = (short)driveData.StartNodeNumber;
                    TagCommand_CtoP.Target_Node = (short)driveData.EndNodeNumber;

                    NewGoCommand = true;
                }
                #endregion

                #region Log
                string strSection = "";
                foreach (SectionData Section in RemainSectionList)
                {
                    strSection += string.Format("Start:{0:D5},End:{1:D5},Link:{2},Steer:{3},Speed:{4:D4},Dist:{5},StartIdx:{6}EndIDx:{7}\r\n"
                                                    , Section.StartNodeNumber
                                                    , Section.EndNodeNumber
                                                    , Section.LinkType
                                                    , Section.SteerDirection
                                                    , Section.Speed
                                                    , Section.Distance
                                                    , Section.tagStartIndex
                                                    , Section.tagEndIndex
                                                    );
                }
                Log(REQ_TYPE.REQ_GOMORE, strSection);
                #endregion
                return RES_TYPE.RES_ACK;
            }
            return RES_TYPE.RES_NACK;
        }

        public RES_TYPE Load(int fromStationNumber, VehDirection direction, int OpPoint, int commnadNumber)
        {
            Station station = SearchHelper.Instance.GetStationByNumber(fromStationNumber);
            if (station != null)
            {
                lock (LockCommand_CtoP)
                {
                    TagCommand_CtoP.Initialize();
                    TagCommand_CtoP.Cmd_Code = (short)CommandCode.Load;
                    TagCommand_CtoP.Direction_Info = (short)direction;
                    TagCommand_CtoP.Active_Pio_NodeNumber = (short)station.Point;
                    TagCommand_CtoP.From_Port = Encoding.UTF8.GetBytes(station.ScreenName.PadRight(16, (char)0x00));
                    TagCommand_CtoP.To_Port = Encoding.UTF8.GetBytes("".PadRight(16, (char)0x00));

                    if (TagOperation_PtoC.Operation.GetBit((int)OperationPLC.From_Transfer_Enable))
                    {
                        Write_IPC_Command();
                        Log(REQ_TYPE.REQ_LOAD, $"/[State] Station Number:{fromStationNumber} / From_Transfer_Enable :true / PLC Value Write Success");
                        return RES_TYPE.RES_ACK;
                    }
                    else
                    {
                        //CancelFrom = true;
                        Log(REQ_TYPE.REQ_LOAD, $"/[State] Station Number:{fromStationNumber} / From_Transfer_Enable : false / PLC Value Write Failed");
                        return RES_TYPE.RES_NACK;
                    }
                }
            }
            else
            {
                Log(REQ_TYPE.REQ_LOAD, $"/[State] Station Number : {fromStationNumber} / Station Object Null");
                return RES_TYPE.RES_NACK;
            }



        }

        public RES_TYPE UnLoad(int toStationNumber, VehDirection direction, int OpPoint, int commnadNumber)
        {
            Station station = SearchHelper.Instance.GetStationByNumber(toStationNumber);
            if (station != null)
            {
                lock (LockCommand_CtoP)
                {
                    TagCommand_CtoP.Initialize();
                    TagCommand_CtoP.Cmd_Code = (short)CommandCode.Unload;
                    TagCommand_CtoP.Direction_Info = (short)direction;
                    TagCommand_CtoP.Active_Pio_NodeNumber = (short)station.Point;
                    TagCommand_CtoP.From_Port = Encoding.UTF8.GetBytes("".PadRight(16, (char)0x00));
                    TagCommand_CtoP.To_Port = Encoding.UTF8.GetBytes(station.ScreenName.PadRight(16, (char)0x00));

                    if (TagOperation_PtoC.Operation.GetBit((int)OperationPLC.To_Transfer_Enable))
                    {
                        Write_IPC_Command();
                        Log(REQ_TYPE.REQ_LOAD, $"/[State] Station Number:{toStationNumber} / To_Transfer_Enable :true / PLC Value Write Success");
                        return RES_TYPE.RES_ACK;
                    }
                    else
                    {
                        //CancelFrom = true;
                        Log(REQ_TYPE.REQ_LOAD, $"/[State] Station Number:{toStationNumber} / To_Transfer_Enable : false / PLC Value Write Failed");
                        return RES_TYPE.RES_NACK;
                    }
                }
            }
            else
            {
                Log(REQ_TYPE.REQ_LOAD, $"/[State] Station Number : {toStationNumber} / Station Object Null");
                return RES_TYPE.RES_NACK;
            }

        }

        public RES_TYPE EStop()
        {
            //모든 태그 데이터 초기화
            Initialize_Tag_CtoP();
            //주행 데이터 초기화
            DriveDataList.Clear();
            RemainSectionList.Clear();
            for (int i = 0; i < SectionMax; i++)
            {
                TagSectionList_CtoP[i].Initialize();
            }

            Write_IPC_Section();
            Write_IPC_Operation(OperationCIM.Estop_Req, true);
            Log(REQ_TYPE.REQ_ESTOP);
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE Reset()
        {
            //모든 태그 데이터 초기화
            Initialize_Tag_CtoP();
            //주행 데이터 초기화
            DriveDataList.Clear();
            RemainSectionList.Clear();
            for (int i = 0; i < SectionMax; i++)
            {
                TagSectionList_CtoP[i].Initialize();
            }

            if (TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Err_Reset_Req))
                Write_IPC_Operation(OperationCIM.Err_Reset_Req, false);
            else
                Write_IPC_Operation(OperationCIM.Err_Reset_Req, true);
            Write_IPC_Section();
            Write_IPC_Command(CommandCode.Reset);
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE Init()
        {
            if (InitCommandTask?.Status == TaskStatus.Running)
                return RES_TYPE.RES_NACK;

            //Drive 데이터 초기화
            DriveDataList.Clear();
            PastPointList.Clear();
            //섹션 정보 초기화
            RemainSectionList.Clear();
            for (int i = 0; i < SectionMax; i++)
            {
                TagSectionList_CtoP[i].Initialize();
            }
            //커맨드 초기화
            Initialize_Tag_CtoP();
            Write_IPC_Command(CommandCode.Init);

            InitCommandTask = Task.Factory.StartNew(() =>
            {
                Stopwatch timeoutChecker = new Stopwatch();
                const int TimeoutLimit = 180000;

                bool Power_On_Status = false;
                bool Power_On_Enable = false;
                bool Power_On_Req = false;

                bool Initial_Status = false;
                bool Initial_Enable = false;
                bool Initial_Req = false;

                bool Auto_Status = false;
                bool Auto_Enable = false;
                bool Auto_Req = false;


                while (true)
                {
                    #region Initialize Bit
                    lock (LockObject_PtoC)
                    {
                        Power_On_Status = TagOperation_PtoC.Operation.GetBit((int)OperationPLC.Power_On_Status);
                        Power_On_Enable = TagOperation_PtoC.Operation.GetBit((int)OperationPLC.Power_On_Enable);

                        Initial_Status = TagOperation_PtoC.Operation.GetBit((int)OperationPLC.Initial_Status);
                        Initial_Enable = TagOperation_PtoC.Operation.GetBit((int)OperationPLC.Initial_Enable);

                        Auto_Status = TagOperation_PtoC.Operation.GetBit((int)OperationPLC.Auto_Status);
                        Auto_Enable = TagOperation_PtoC.Operation.GetBit((int)OperationPLC.Auto_Enable);
                    }

                    lock (LockOperation_CtoP)
                    {
                        Power_On_Req = TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Power_On_Req);
                        Initial_Req = TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Initial_Req);
                        Auto_Req = TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Auto_Req);
                    }
                    #endregion
                    #region Initializes Timeout Check
                    if (timeoutChecker.IsRunning == false)// while 첫 진입시 
                    {
                        timeoutChecker.Restart();
                        Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Initializes Start");
                    }
                    else
                    {
                        if (timeoutChecker.ElapsedMilliseconds > TimeoutLimit)
                        {
                            Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Initializes Timeout");
                            break; //Thread Exit
                        }
                    }
                    #endregion
                    #region Initializes Sequence
                    // Power On 확인
                    if (Power_On_Status)
                    {
                        if (Power_On_Req) // Servo On Request가 켜져있다면
                        {
                            Write_IPC_Operation(OperationCIM.Power_On_Req, false);
                            Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Initializing / [Bit] Power_On_Req:false");
                        }
                        // Init 확인
                        if (Initial_Status)
                        {
                            // Auto 확인
                            if (Auto_Status)
                            {
                                timeoutChecker.Stop();
                                #region  Request Bit OFF 처리
                                if (Auto_Req)
                                {
                                    Write_IPC_Operation(OperationCIM.Auto_Req, false);
                                    Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Auto_Status true / [Bit] Auto_Req:true -> false");
                                }
                                if (Initial_Req)
                                {
                                    Write_IPC_Operation(OperationCIM.Initial_Req, false);//lgy 이게 왜 여기서 처리되는지 모르겠다.. 이유가 따로있나
                                    Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Auto_Status true / [Bit] Initial_Req:true -> false");
                                }
                                #endregion
                                Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Initializes Success");
                                break; //Thread Exit
                            }
                            //Auto 상태가 아닐때
                            else
                            {
                                if (Auto_Enable && !Auto_Req)// Auto_Enable이 켜져있고 Auto_Req 가 꺼져있다면
                                {
                                    Thread.Sleep(500);
                                    Write_IPC_Operation(OperationCIM.Auto_Req, true);
                                    Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Auto_Status false / [Bit] Auto_Req:false -> true");
                                }
                            }
                        }
                        //Init 상태가 아닐때
                        else
                        {
                            if (Initial_Enable && !Initial_Req)// Init Enable이 켜져있고 Init Request가 꺼져있다면
                            {
                                Thread.Sleep(500);
                                Write_IPC_Operation(OperationCIM.Initial_Req, true);
                                Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Initial_Status false /[Bit] Initial_Req:false -> true");
                            }
                        }
                    }
                    // Power On 상태가 아닐때
                    else
                    {
                        if (Power_On_Enable && !Power_On_Req) // Power_On_Enable 이 켜져있고 Power_On_Req 가 꺼져있다면
                        {
                            Write_IPC_Operation(OperationCIM.Power_On_Req, true);
                            Log(REQ_TYPE.REQ_INIT, "/ [State]:PLC Power_On_Status false /[Bit] Power_On_Req:false -> true");
                        }
                    }
                    #endregion
                    Thread.Sleep(250);
                }
            });
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE Cancel()
        {
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE Run()
        {
            Write_IPC_Operation(OperationCIM.Pause_Req, false);
            Log(REQ_TYPE.REQ_RUN);
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE Stop()
        {
            Write_IPC_Operation(OperationCIM.Pause_Req, true);
            Log(REQ_TYPE.REQ_STOP);
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE ServoOn()
        {
            Write_IPC_Operation(OperationCIM.Power_On_Req, true);
            Write_IPC_Operation(OperationCIM.Power_Off_Req, false);
            Log(REQ_TYPE.REQ_SERVO_ON);
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE ServoOff()
        {
            Write_IPC_Operation(OperationCIM.Power_On_Req, false);
            Write_IPC_Operation(OperationCIM.Power_Off_Req, true);
            Log(REQ_TYPE.REQ_SERVO_OFF);
            return RES_TYPE.RES_ACK;
        }

        public RES_TYPE TimeSync()
        {
            return RES_TYPE.RES_ACK;
        }
        #endregion

        public void DriveSection()
        {
            StringBuilder logMsg_AllocateSection = new StringBuilder();
            lock (CoreController.Instance.LockDriveData)
            {
                #region 주행데이터 존재 확인
                if (RemainSectionList.Count == 0)
                {
                    SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, "DriveData Empty");
                    foreach (var section in TagSectionList_CtoP)
                    {
                        section.Initialize();
                    }
                    Write_IPC_Section();
                    return;
                }
                else if (WriteLastIndex == RemainSectionList.LastOrDefault().Section_Idx) /// 주행데이터 처리 완료.
                {
                    foreach (var section in TagSectionList_CtoP)
                    {
                        section.Initialize();
                    }
                    Write_IPC_Section();
                    return;
                }
                #endregion

                #region 변수 초기화
                foreach (var section in TagSectionList_CtoP)
                {
                    section.Initialize();
                }

                int SumDistance = 0;
                int WriteIndex = 0;
                int EmptyCount = 0;
                int index = 0;
                int WriteBufferStartIndex = 0;
                int WriteBufferEndIndex = 0;
                int WriteBufferCount = 0;
                //string FromPortName, ToPortName;
                var search = SearchHelper.Instance;

                // WriteButter Start,End Index Setting
                foreach (var TagSection in TagSectionList_CtoP)
                {
                    if (TagSection.Section_Idx > 0)
                    {
                        ++WriteBufferCount;

                        if (WriteBufferStartIndex == 0)
                            WriteBufferStartIndex = TagSection.Section_Idx;

                        if (WriteBufferEndIndex < TagSection.Section_Idx)
                            WriteBufferEndIndex = TagSection.Section_Idx;
                    }
                }

                //WriteLastIndex, EmptyCount Setting 
                foreach (var TagSection in TagSectionList_PtoC)
                {
                    if (TagSection.Section_Idx > WriteLastIndex)
                    {
                        WriteLastIndex = TagSection.Section_Idx; //전체 섹션중 가장 큰값
                    }
                    // 거리가 0이면 빈 곳일테니, 비어있는 섹션 갯수 확인, GoMore 판정용.
                    if (TagSection.Distance == 0)
                    {
                        EmptyCount++;
                    }
                }
                #endregion

                #region PLC 접수 및 인덱스 확인(섹션데이터 어디까지 가져갔는지와 비어있는 갯수 확인),
                // PLC에서 가져간거 확인한 부분. 주행데이터에서 LastIndex 보다 작은 index가 존재한다면 Alloc 처리
                foreach (var sectionData in RemainSectionList)
                {
                    if (sectionData.Section_Idx <= WriteLastIndex )
                    {
                        sectionData.Alloc = true; //가져갔다는 뜻
                    }
                }
                #endregion

                #region 버퍼에 써논것과 PLC에서 가져간 부분을 확인 처리할 곳,
                // 가져갔는지, 안가져갔는지, 일부만 가져갔는지 등...
                if (WriteBufferEndIndex == WriteLastIndex)
                {
                    // 써준 버퍼 섹션을 모두 가져갔음 OK. 다시 써줄게 있는지 확인하러...
                }
                else if (WriteBufferCount > 0 && WriteBufferStartIndex < WriteLastIndex)
                {
                    // 버퍼 섹션을 일부만 가져갔음, 이상함.. 기다리자.
                    SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"Wait Allocate PLC(Index) WriteStartIdx:{WriteBufferStartIndex}/WriteBufEndIdx:{WriteBufferEndIndex}/PLC_WriteLastIndex{WriteLastIndex}");
                    return;
                }
                else
                {
                    if (TagCommand_CtoP.Total_Distance > 0 && TagCommand_CtoP.Total_Distance != TagCommand_PtoC.Total_Distance)
                    {
                        SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"Wait Allocate PLC(Distance) CurrDistance:{TagCommand_CtoP.Total_Distance}/m_CmdData_Moni Distance:{TagCommand_PtoC.Total_Distance}");
                        return;
                    }
                    // 버퍼 섹션을 안가져갔음. 버퍼에 썼는데 안가져가고 있으니 기다리겠음. 
                    if (WriteBufferCount == EmptyCount)
                    {
                        SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"Wait Allocate PLC(EmptyCount) WriteStartIdx:{WriteBufferStartIndex}/WriteBufEndIdx:{WriteBufferEndIndex}/PLC_WriteLastIndex{WriteLastIndex}");
                        return;
                    }
                    // 양쪽 버퍼 카운트에 변화가 있다면 재연산. 
                }
                #endregion

                #region 섹션 데이터 처리 v2
                WriteIndex = 0;
                SumDistance = 0;
                foreach (SectionData sectionData in RemainSectionList)
                {
                    if (EmptyCount == 0) // 버퍼가 다 차있는 상황
                        break;
                    if (WriteStartIndex <= sectionData.Section_Idx) // 거리데이터 처리
                        SumDistance += sectionData.Distance;
                    #region 미할당 섹션 할당
                    if (sectionData.Alloc == false)  // 미할당 섹션이 있다.
                    {
                        if (0 == WriteIndex) // 시작노드값                      
                            TagCommand_CtoP.Start_Node = (short)sectionData.DriveSegmentList[0].StartPnt;

                        Tag_Section newTagSection = new Tag_Section
                        {
                            Distance = sectionData.Distance,
                            Speed = (short)sectionData.SpeedOrg,
                            Drive_Type = (short)(sectionData.LinkType)
                        }; //할당할 새섹션 생성

                        #region Steer 처리                                                
                        switch (sectionData.SteerDirection)
                        {
                            case SteerDir.Left:
                                newTagSection.Steer_Direction = 1; break;
                            case SteerDir.Right:
                                newTagSection.Steer_Direction = 2; break;
                            case SteerDir.None:
                            case SteerDir.DontCare:
                            default: newTagSection.Steer_Direction = 3; break;
                        }

                        switch (sectionData.NextSteerDirection)
                        {
                            case SteerDir.Left:
                                newTagSection.NextSteer_Direction = 1; break;
                            case SteerDir.Right:
                                newTagSection.NextSteer_Direction = 2; break;
                            case SteerDir.None:
                            case SteerDir.DontCare:
                            default: newTagSection.NextSteer_Direction = 3; break;
                        }
                        #endregion

                        #region 할당할 새섹션에 현재 sectionData의 PathPoint 넘버와 길이 값을 초기화
                        foreach (Point PathPoint in sectionData.DrivePointList)
                        {
                            newTagSection.Node_Number[index] = (short)PathPoint.BcrNumber; //번호 초기화

                            if (sectionData.DriveSegmentList[index].SegmentLength < ushort.MaxValue) //길이 초기화
                                newTagSection.Node_Distance[index] = (ushort)sectionData.DriveSegmentList[index].SegmentLength;
                            else
                                SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.WARN, $"OverLength {sectionData.DriveSegmentList[index].SegmentLength}");
                            index++; //Index Increase
                        }
                        #endregion

                        newTagSection.Node_Count = (short)index;
                        newTagSection.Section_Idx = (short)sectionData.Section_Idx;

                        TagSectionList_CtoP[WriteIndex] = newTagSection;

                        WriteIndex++;

                        WriteLastIndex = sectionData.Section_Idx;

                        // 빈버퍼갯수만큼 다 써줬음
                        if (EmptyCount == WriteIndex)
                            break;
                    }
                    #endregion
                }
                #endregion

                //이미 주행중거나 빈 버퍼가 있었다면 고모어 커맨드로 처리
                if ((TagCommand_CtoP.Cmd_Code == (short)CommandCode.Go) || (TagCommand_PtoC.Cmd_Code == (short)CommandCode.GoMore))
                {
                    if ((WriteLastIndex > 0) && (EmptyCount < 5) && (WriteIndex > 0))
                    {
                        lock (LockCommand_CtoP)
                        {
                            TagCommand_CtoP.Cmd_Code = (short)CommandCode.GoMore;
                            TagCommand_CtoP.Total_Distance = SumDistance;
                            TagCommand_CtoP.Section_Count = (short)WriteIndex;
                            TagCommand_CtoP.Target_Node = TagSectionList_CtoP[WriteIndex - 1].Node_Number[index - 1];

                            CurrentSpeed = TagSectionList_CtoP[WriteIndex - 1].Speed;
                            ////받은 주행명령 기반으로 기타 정보.
                            //CommandData FirstCmd = m_lstCommandList.FirstOrDefault();
                            //CommandData LastCmd = m_lstCommandList.LastOrDefault();

                            //FromPortName = FirstCmd.FromPnt.ToString();
                            //ToPortName = LastCmd.ToPnt.ToString();

                            //ChkCmdFromTo(ref CmdData_Curr, FromPortName, ToPortName);
                            //m_TotalDistance = SumDistance;
                        }

                        Write_IPC_Section();
                        Thread.Sleep(500);
                        Write_IPC_Command();

                        SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"GoMore WriteCount:{WriteIndex}/LastIdx:{TagSectionList_CtoP[WriteIndex - 1].Section_Idx}/Distance:{SumDistance}");
                        return;
                    }
                }

                if (WriteIndex > 0)
                {
                    //LogMsg_ChangedWriteIdx = true;
                    lock (LockCommand_CtoP)
                    {
                        // 디스플레이는 마지막 이적재 기준으로.
                        TagCommand_CtoP.Cmd_Code = (short)CommandCode.Go;
                        TagCommand_CtoP.Section_Count = (short)WriteIndex;
                        TagCommand_CtoP.Target_Node = TagSectionList_CtoP[WriteIndex - 1].Node_Number[index - 1];

                        WriteStartIndex = (short)TagSectionList_CtoP[0].Section_Idx;

                        SumDistance = 0;
                        foreach (var section in TagSectionList_CtoP)
                        {
                            SumDistance += section.Distance;
                        }

                        TagCommand_CtoP.Total_Distance = SumDistance;

                        ////받은 주행명령 기반으로 기타 정보.
                        //CommandData FirstCmd = m_lstCommandList.FirstOrDefault();
                        //CommandData LastCmd = m_lstCommandList.LastOrDefault();
                        //Station TargetStation = null;

                        ////스테이션 넘버가 있다면,
                        //if (LastCmd.StationNumber > 0)
                        //{
                        //    TargetStation = search.GetStationByNumber(LastCmd.StationNumber);
                        //}

                        //FromPortName = FirstCmd.FromPnt.ToString();

                        //if (null != TargetStation)
                        //    ToPortName = TargetStation.ScreenName;
                        //else
                        //    ToPortName = LastCmd.ToPnt.ToString();

                        //TagCommand_CtoP.Moving_Type = (short)LastCmd.MoveType;

                        //m_TotalDistance = SumDistance;
                        NewGoCommand = false;
                    }

                    // 일반 고일경우
                    Write_IPC_Section();
                    Thread.Sleep(500);
                    Write_IPC_Command();

                    SSLogManager.Instance.Write(SSLog.LogType.PLC, LogLevel.INFO, $"Go WriteCount:{WriteIndex}/LastIdx:{TagSectionList_CtoP[WriteIndex - 1].Section_Idx}/Distance:{SumDistance}");
                    return;
                }

                if (WriteIndex == 0) //섹션 데이터가 존재하지 않는다.
                {
                    Write_IPC_Section();
                }
            }//Lock
        }

        internal bool CheckGoingCommand()
        {
            short GO = (short)CommandCode.Go;
            short GOMORE = (short)CommandCode.GoMore;

            if (TagCommand_PtoC.Cmd_Code == GO || TagCommand_PtoC.Cmd_Code == GOMORE ||
               TagCommand_CtoP.Cmd_Code == GO || TagCommand_CtoP.Cmd_Code == GOMORE || NewGoCommand == true)
            {
                return true;
            }
            return false;
        }

        private List<SectionData> MakeMaxNodeSectionDataList(DriveData driveData, short NodeStartIdx = 0, short SectStartIdx = 0)
        {
            List<SectionData> NewSectionList = new List<SectionData>();

            short SectionIdx = SectStartIdx;
            int TotalNodeIdx = NodeStartIdx;
            // 섹션 정보 입력
            // 기존 주행 정보엔 섹션 노드 갯수 제한이 없었으므로, 최대 노드 갯수(MaxNodeCount)에 따른 색션 처리가 필요하다.
            // defualt 20개의 섹션까지만으로 생성한다.
            foreach (var SourceSection in driveData.SectionList)
            {
                // 존(노드, 포인트) 인덱스용
                int NodeIdx = 0;
                int SumDistance = 0;

                var NewSectionData = new SectionData();
                ++SectionIdx;
                ///주행 포인트
                foreach (Point PathPoint in SourceSection.DrivePointList)
                {
                    ++TotalNodeIdx;
                    if (0 == NodeIdx % ConfigData.Instance.MaxNodeCount)
                    {
                        //최대 노드 갯수에 따른 섹션 증가
                        if (NodeIdx > 0)
                        {
                            // 기존 섹션 데이터 처리
                            NewSectionData.Distance = SumDistance;
                            NewSectionList.Add(NewSectionData);

                            // 새 섹션
                            NewSectionData = new SectionData();
                            ++SectionIdx;
                        }

                        //섹션당 같은 부분
                        SumDistance = 0;
                        NewSectionData.StartNodeNumber = (int)PathPoint.BcrNumber; //lgy BcrNumber 넣는게 맞는건가?
                        NewSectionData.tagStartIndex = TotalNodeIdx;
                        NewSectionData.Section_Idx = (short)(SectionIdx);
                        NewSectionData.SpeedOrg = SourceSection.SpeedOrg;
                        NewSectionData.Speed = SourceSection.Speed;
                        NewSectionData.LinkType = SourceSection.LinkType;// 1:Line, 2:Arc , 0:None, 
                        NewSectionData.SteerDirection = SourceSection.SteerDirection;// 0:None, 1:Left 2:Right , 3:DontCare
                    }

                    // 포인트, 세그먼트 넘김
                    NewSectionData.tagEndIndex = TotalNodeIdx;
                    NewSectionData.EndNodeNumber = (int)PathPoint.BcrNumber;
                    NewSectionData.DrivePointList.Add(PathPoint);
                    NewSectionData.DriveSegmentList.Add(SourceSection.DriveSegmentList[NodeIdx]);
                    SumDistance += (int)SourceSection.DriveSegmentList[NodeIdx].SegmentLength;

                    ++NodeIdx;
                }

                NewSectionData.Distance = SumDistance;
                NewSectionList.Add(NewSectionData);
            }

            int GetIdx = 0;
            foreach (var newSection in NewSectionList)
            {
                ++GetIdx;
                if (NewSectionList.Count > GetIdx)
                {
                    newSection.NextSteerDirection = NewSectionList[GetIdx].SteerDirection;
                }
                else
                    newSection.NextSteerDirection = driveData.LastSteerDir;
            }

            return NewSectionList;
        }

        private List<SectionData> MakeSectionDataList(List<Point> pathPoint_List, List<Segment> pathSegment_List)
        {
            List<SectionData> SectionList = new List<SectionData>();

            SectionData Old_DriveSection = null;
            SectionData Cur_DriveSection = null;

            int TagCount = 0;
            bool NewSection = false;
            short SectionIndex = 1;

            #region PathSegmanet를 토대로 SectionDataList 세팅

            for (int idx = 0; idx < pathSegment_List.Count; ++idx)
            {
                Cur_DriveSection = new SectionData();

                NewSection = false;
                Segment Segment = pathSegment_List[idx];
                Point Point_Start = SearchHelper.Instance.GetPointByNum((int)Segment.StartPnt);
                Point Point_End = SearchHelper.Instance.GetPointByNum((int)Segment.EndPnt);

                Cur_DriveSection.DriveSegmentList.Add(Segment);
                Cur_DriveSection.DrivePointList.Add(Point_End);
                int nTest = Cur_DriveSection.DriveSegmentList.Count;

                Cur_DriveSection.StartNodeNumber = pathPoint_List[idx].Number;
                Cur_DriveSection.EndNodeNumber = pathPoint_List[idx + 1].Number;
                Cur_DriveSection.LinkType = (LineArc)Segment.LineType;
                Cur_DriveSection.SteerDirection = (SteerDir)Segment.SteerDir;
                Cur_DriveSection.SpeedOrg = Cur_DriveSection.Speed = (int)Segment.Speed;
                Cur_DriveSection.Distance = (int)Segment.SegmentLength;
                Cur_DriveSection.Section_Idx = SectionIndex;

                /// 다음 포인트, 가상바코드 체크
                /// Tag 카운트 설정

                if ((int)SSLib.PointType.Virtual != Point_End.PointType)     /// 일반
                {
                    TagCount++;
                }
                else //if (OCS.PointType.Virtual== Point_End.Type)     /// 가상바코드일경우 
                {
                    if (null != Old_DriveSection)
                    {
                        Old_DriveSection.Distance += Cur_DriveSection.Distance;

                    }
                    continue;
                }


                Cur_DriveSection.tagEndIndex = TagCount;

                /// 변경사항 검사하여 섹션 분리 여부 판단
                if (null == Old_DriveSection)
                {
                    NewSection = true;

                    //처음일 경우
                    if (Cur_DriveSection.SteerDirection == SteerDir.None || Cur_DriveSection.SteerDirection == SteerDir.DontCare)
                    {
                        for (int stidx = 0; stidx < pathSegment_List.Count; ++stidx)
                        {
                            if (pathSegment_List[stidx].SteerDir == (int)SteerDir.Right || pathSegment_List[stidx].SteerDir == (int)SteerDir.Left)
                            {
                                Cur_DriveSection.SteerDirection = (SteerDir)pathSegment_List[stidx].SteerDir;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (Old_DriveSection.LinkType != Cur_DriveSection.LinkType) NewSection = true;
                    if (Old_DriveSection.Speed != Cur_DriveSection.Speed) NewSection = true;

                    /// 스티어링 조건 확인, N(상관없음) 일경우 조건이 있는 쪽을 따라간다
                    /// 
                    if (Old_DriveSection.SteerDirection != Cur_DriveSection.SteerDirection)
                    {
                        if (Cur_DriveSection.SteerDirection == SteerDir.None || Cur_DriveSection.SteerDirection == SteerDir.DontCare)
                        {
                            //스티어 상관없음일 경우 뒤에 섹션 우선으로 값넣어줌
                            for (int IdxSteer = idx + 1; IdxSteer < pathSegment_List.Count; ++IdxSteer)
                            {
                                if (pathSegment_List[IdxSteer].SteerDir == (int)SteerDir.Right || pathSegment_List[IdxSteer].SteerDir == (int)SteerDir.Left)
                                {
                                    Cur_DriveSection.SteerDirection = (SteerDir)pathSegment_List[IdxSteer].SteerDir;
                                    break;
                                }
                            }
                            // 뒷 섹션을 검색하고도 상관없음이면 앞섹션 스티어로.
                            if (Cur_DriveSection.SteerDirection == SteerDir.None || Cur_DriveSection.SteerDirection == SteerDir.DontCare)
                            {
                                Cur_DriveSection.SteerDirection = Old_DriveSection.SteerDirection;
                            }
                        }
                        else
                        {
                            NewSection = true;
                        }
                    }

                    if (Old_DriveSection.SteerDirection != Cur_DriveSection.SteerDirection) NewSection = true;
                }

                /// 섹션 분리여부에 따른 데이터 분류
                if (NewSection)
                {
                    Cur_DriveSection.tagStartIndex = TagCount;

                    SectionList.Add(Cur_DriveSection);

                    Old_DriveSection = SectionList.LastOrDefault();

                    ++SectionIndex;
                }
                else
                {
                    Old_DriveSection.SteerDirection = Cur_DriveSection.SteerDirection;
                    Old_DriveSection.EndNodeNumber = Cur_DriveSection.EndNodeNumber;
                    Old_DriveSection.tagEndIndex = Cur_DriveSection.tagEndIndex;
                    Old_DriveSection.Distance += Cur_DriveSection.Distance;
                    Old_DriveSection.DriveSegmentList.Add(Segment);
                    Old_DriveSection.DrivePointList.Add(Point_End);
                    Old_DriveSection.Section_Idx = Cur_DriveSection.Section_Idx;
                }
            }
            #endregion

            return SectionList;
        }

        #region Log

        private void Log(REQ_TYPE commandType, string addContent = "")
        {
            string content = $"[Command] : {commandType.ToString()} ";
            switch (commandType)
            {
                case REQ_TYPE.REQ_SERVO_ON:
                case REQ_TYPE.REQ_SERVO_OFF:
                    content += $" / [Power_On_Req] : {TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Power_On_Req)} / [Power_Off_Req] : {TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Power_Off_Req)}";
                    break;
                case REQ_TYPE.REQ_RUN:
                case REQ_TYPE.REQ_STOP:
                    content += $" / [Pause_Req] : {TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Pause_Req)}";
                    break;
                case REQ_TYPE.REQ_INIT:
                    break;
                case REQ_TYPE.REQ_ESTOP:
                    content += $" / [Estop_Req] : {TagOperation_CtoP.Operation.GetBit((int)OperationCIM.Estop_Req)}";
                    break;
                case REQ_TYPE.REQ_GO:
                    break;
                case REQ_TYPE.REQ_GOMORE:
                    break;
                case REQ_TYPE.REQ_LOAD:
                    break;
                case REQ_TYPE.REQ_UNLOAD:
                    break;
                case REQ_TYPE.REQ_RESET:
                    break;
                case REQ_TYPE.REQ_CANCEL:
                    break;
                case REQ_TYPE.REQ_TIME_SYNC:
                    break;
                default:
                    break;
            }
            content += addContent;
            SSLogManager.Instance.Write(nameof(MotionController), LogLevel.INFO, content);
        }
        #endregion

    }
}
