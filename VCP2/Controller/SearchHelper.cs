﻿using OCS.VCP.Config;
using SSDatabase;
using SSDatabase.Models.RunData;
using SSLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCS.VCP.Controller
{

    class SearchHelper
    {
        #region Singleton
        private static volatile SearchHelper instance;

        public static SearchHelper Instance
        {
            get
            {
                if (instance == null)
                    instance = new SearchHelper();
                return instance;
            }
        }
        #endregion

        #region Field
        public List<Station> StationList { get; private set; }
        public List<Point> PointList { get; private set; }
        public List<Segment> SegmentList { get; private set; }
        #endregion

        #region Property
        #endregion

        private SearchHelper()
        {
        }

        public void Initializes()
        {
            StationList = SSDBManager.Instance.GetStationList("127.0.0.1");
            PointList = SSDBManager.Instance.GetPointList("127.0.0.1");
            SegmentList = SSDBManager.Instance.GetSegmentList("127.0.0.1");
            if (StationList == null || PointList == null || SegmentList == null)
            {
                SSLogManager.Instance.Exception($"Data loaded from database is Null. / StationList is null : {StationList == null}, PointList is null : {PointList == null}, SegmentList is null : {SegmentList == null}");
                throw new NullReferenceException($"Data loaded from database is Null. / StationList is null : {StationList == null}, PointList is null : {PointList == null}, SegmentList is null : {SegmentList == null}");
            }
        }

        public Station GetStationByNumber(int number)
        {
            return StationList.Where(s => s.Number == number).FirstOrDefault();
        }

        public int GetIndexByPointNumber(int number)
        {
            return PointList.FindIndex(p => p.Number == number);
        }

        public Point GetPointByBCR(int BCR)
        {
            if (0 >= BCR) return null;
            return PointList.Where(point => point.BcrNumber == BCR).FirstOrDefault();
        }

        public Point GetPointByNum(int pointNum)
        {
            return PointList.Where(point => point.Number == pointNum).FirstOrDefault();
        }

        public bool SetSegmentListByPoints(List<Point> pathPoint_List, ref List<Segment> pathSegment_List)
        {
            for (int idx = 1; idx < pathPoint_List.Count; ++idx)
            {
                Segment segment = GetSegmentByFromTo(pathPoint_List[idx - 1].Number, pathPoint_List[idx].Number);

                if (null == segment)
                {
                    return false;
                }
                else
                {
                    pathSegment_List.Add(segment);
                }
            }
            return true;
        }

        public Segment GetSegmentByFromTo(int from, int to)
        {
            Point point = this.GetPointByNum(from);

            if (point != null)
            {
                if(point.SegOut.Contains(","))
                {
                    string[] segOut = point.SegOut.Split(',');

                    foreach (var SegOutNum in segOut)
                    {
                        Segment Segment = GetSegmentByNumber(int.Parse(SegOutNum));

                        if (Segment != null && Segment.EndPnt == to)
                            return Segment;
                    }
                }
                else
                {
                    Segment Segment = GetSegmentByNumber(int.Parse(point.SegOut));

                    if (Segment != null && Segment.EndPnt == to)
                        return Segment;
                }

            }

            return null;
        }
        public Segment GetSegmentByNumber(int number)
        {
            return SegmentList.Where(seg => seg.Number == number).FirstOrDefault();
        }

        public List<Segment> GetSegmentListBySegOutString(string segOutString)
        {
            List<Segment> segmentList = new List<Segment>();
            List<int> segmentNumberList = new List<int>();
            var segmentNumber = segOutString.Split(',');
            foreach (var number in segmentNumber)
            {
                segmentList.Add(GetSegmentByNumber(int.Parse(number)));
            }
            return segmentList;
        }
        
    }
}
