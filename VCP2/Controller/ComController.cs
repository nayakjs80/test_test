﻿using OCS.VCP.Config;
using OCS.VCP.Model.TagSet;
using OCSProtocol.Model;
using OCSProtocol.OcsSocket;
using SSLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OCS.VCP.Controller
{
    public sealed class ComController
    {
        #region Singleton
        private static volatile ComController instance;
        private static object syncRoot = new object();

        public static ComController Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ComController();
                    }
                }

                return instance;
            }
        }

        public bool IsCoreConnected { get; private set; }

        #endregion

        private Client client;
        private Timer TryConnectTimer;
        private Timer NotificationTimer;

        private ComController()
        {
            client = new Client();

            client.Connected += Client_Connected;
            client.Disconnected += Client_Disconnected;
            client.RequestReceived += Client_RequestReceived;
            client.ResponseReceived += Client_ResponseReceived;
            client.ErrorOccured += Client_ErrorOccured;
            
            TryConnectTimer = new Timer(5 * 1000);
            TryConnectTimer.Elapsed += TryConnectTimer_Elapsed;

            NotificationTimer = new Timer(100);
            NotificationTimer.Elapsed += NotificationTimer_Elapsed;
        }

        public void Initializes()
        {
            TryConnectTimer.Start();

            Connect();
        }

        private async void Connect()
        {
            try
            {
                var result = await client.ConnectAsync(ConfigData.Instance.ServerIP, ConfigData.Instance.ServerPort);

                if (result)
                {
                    SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, "Core Server Connected");
                    SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, "[Send] InitializeData Core Server");
                    client.SendData(new InitializeData() { VehicleNumber = ConfigData.Instance.VehicleNumber });
                }
                else
                {
                    SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, "Core Server Connect Fail.");
                }
            }
            catch (Exception ex)
            {
                SSLogManager.Instance.Exception(ex.Message + " " + ex.StackTrace);
            }

        }


        private void TryConnectTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (client.IsConnected == false)
            {
                Connect();
            }
        }

        private void NotificationTimer_Elapsed(object sender, ElapsedEventArgs e)
        {   
            var Core = CoreController.Instance;
            NotificationData data = new NotificationData()
            {
                VehicleNumber = ConfigData.Instance.VehicleNumber,
                CST_Contain = Core.CST_Contain,
                SoonArrive = Core.SoonArrive,
                CST_ID = Core.CST_ID,
                Distance = Core.Distance,
                IO_1 = Core.IO_1,
                IO_2 = Core.IO_2,
                IO_3 = Core.IO_3,
                IsError = Core.IsError,
                StartNode = Core.BeforeNode,
                EndNode = Core.CurrentNode,
                ErrorNumberArray = Core.ErrorCodeArray,
                Mode = Core.Mode,
                State = Core.VehicleState,
                IsPLCEnable = MotionController.Instance.ConnectionState
            };
            client.SendData(data);
            IsCoreConnected = true;
        }

        private void Client_ErrorOccured(object sender, OCSProtocol.ErrorArgs e)
        {
            IsCoreConnected = false;
        }

        private void Client_ResponseReceived(object sender, ResponseData e)
        {
            if (e.ResponseType == RES_TYPE.RES_INIT_ACK)
            {
                NotificationTimer.Start();
                SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, $"[Received] InitializeData Result : {e.ResponseType.ToString()}");
            }
            else
            {
                IsCoreConnected = false;
                NotificationTimer.Stop();
                SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, $"[Received] InitializeData Result : {e.ResponseType.ToString()}");
            }
        }

        private void Client_RequestReceived(object sender, RequestData e)
        {
            //명령을 생성해서 큐에 넣을까?
            //바로 수행하고 응답 할까?
            //PLC bit만 살리는 애들은 명령 생성할 필요가 있을까?
            var motion = MotionController.Instance;

            SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, $"[Received] Command Request : {e.RequestType.ToString()} Command Number [{e.CommandNumber}]");
            RES_TYPE result = RES_TYPE.RES_NACK;
            switch (e.RequestType)
            {
                case REQ_TYPE.REQ_GO:
                    result = motion.Go(CoreController.Instance.LastEndPointNumber, e.OpPoint, e.MoveCommandType);
                    break;
                case REQ_TYPE.REQ_GOMORE:
                    result = motion.GoMore(e.OpPoint,e.MoveCommandType);
                    break;
                case REQ_TYPE.REQ_LOAD:
                    result = motion.Load(e.FromStationNumber,e.Direction,e.OpPoint,e.CommandNumber);
                    break;
                case REQ_TYPE.REQ_UNLOAD:
                    result = motion.UnLoad(e.ToStationNumber,e.Direction,e.OpPoint,e.CommandNumber);
                    break;
                case REQ_TYPE.REQ_ESTOP:
                    result = motion.EStop();
                    break;
                case REQ_TYPE.REQ_RESET:
                    result = motion.Reset();
                    break;
                case REQ_TYPE.REQ_INIT:
                    result = motion.Init();
                    break;
                case REQ_TYPE.REQ_CANCEL:
                    result = motion.Cancel();
                    break;
                case REQ_TYPE.REQ_RUN:
                    result = motion.Run();
                    break;
                case REQ_TYPE.REQ_STOP:
                    result = motion.Stop();
                    break;
                case REQ_TYPE.REQ_SERVO_ON:
                    result = motion.ServoOn();
                    break;
                case REQ_TYPE.REQ_SERVO_OFF:
                    result = motion.ServoOff();
                    break;
                case REQ_TYPE.REQ_TIME_SYNC:
                    result = motion.TimeSync();
                    break;
                default:
                    break;
            }
            client.SendData(new ResponseData() { VehicleNumber = ConfigData.Instance.VehicleNumber, RequestType = e.RequestType, ResponseType = result });
        }

        private void Client_Disconnected(object sender, EventArgs e)
        {
            IsCoreConnected = false;
            SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, $"[Recevied] Client Disconnect Event.");
            NotificationTimer.Stop();
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            IsCoreConnected = true;
            SSLogManager.Instance.Write(nameof(ComController), LogLevel.INFO, $"[Recevied] Client Connect Event.");
        }
        
    }
}
